/**This is the program that calculates 2 operand that you enter.
 * there are 4 operators sample add(+),subtract(-),multiply(*)and divide(/)
 * Writer : Ms.Supitsara Deetoh
 * ID : 603040469-0
 * sec : 2
 Date : 19 Jan 2018*/
package deetoh.supitsara.lab2;

public class Calculator {
	public static void main(String[] x){
		if (x.length < 3 ) { //that's for entering operand less than 3. 
			System.err.print("Calculator <operand1> <operand2> <operator>");}
		else{
			if (x[2].equals("+")) {
				float operand1 = Float.parseFloat(x[0]);
				float operand2 = Float.parseFloat(x[1]);
				float result = operand1+operand2;
				System.out.printf("%.1f + %.1f = %.1f",operand1,operand2,result);
			}else if (x[2].equals("-")){
				float operand1 = Float.parseFloat(x[0]);
				float operand2 = Float.parseFloat(x[1]);
				float result = operand1-operand2;
				System.out.printf("%.1f - %.1f = %.1f",operand1,operand2,result);
			}else if (x[2].equals("*") || x[2].equals(".classpath")) {
				float operand1 = Float.parseFloat(x[0]);
				float operand2 = Float.parseFloat(x[1]);
				double result = operand1*operand2;
				System.out.printf("%.1f * %.1f = %.1f",operand1,operand2,result);
			}else if (x[2].equals("/")){
				if (x[1]=="0"){
					System.out.print("The second operand cannot be zero");}
				float operand1 = Float.parseFloat(x[0]);
				float operand2 = Float.parseFloat(x[1]);
				float result = operand1/operand2;
				System.out.printf("%.1f / %.1f = %.2f",operand1,operand2,result);
			}
			else {
				System.err.print("Calculator <operand1> <operand2> <operator>");
				/**This line'll work sample you enter variable more than 2 
				/*or the third variable aren't add(+),subtract(-),multiply(*)and divide(/)
				 *or the first and third operands can't calculate as well
				 *or other.
				 */
			}
		
		}
		
	}
	
}