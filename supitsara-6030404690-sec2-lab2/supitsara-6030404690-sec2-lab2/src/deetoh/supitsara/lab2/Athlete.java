/**This is the program'll display your infomation favorite athlete sample name sport and nation.
 * Writer : Ms.Supitsara Deetoh
 * ID : 603040469-0
 * sec : 2
 Date : 15 Jan 2018*/
package deetoh.supitsara.lab2;

public class Athlete {
	public static void main(String[] x) {
		
		if (x.length == 3) {
			String name = x[0];
			String sport = x[1];
			String nation = x[2];
			
			System.out.println("My favorite athlete is "+name+" who plays "+sport+" and has nationality as "+nation);
		
		
		} else {
			System.out.println("Athlete <athelete name> <sport name> <nationality>") ; }
	}
}