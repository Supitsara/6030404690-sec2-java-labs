/**This is the program that checks sub of messages.
 * you'll enter the first variable to be checker and other variable to be messages.
 * Writer : Ms.Supitsara Deetoh
 * ID : 603040469-0
 * sec : 2
 Date : 19 Jan 2018*/
package deetoh.supitsara.lab2;

public class SubstringChecker {
	public static void main(String[] x) {
		if ( x.length < 2 ){
			System.err.print("SubstringChecker <substring> <str1> ...");
			
		}else{ //This case for x.length >= 2
			int i; //decare variable for count in for-loop
			int count = 0;//total round for displaying in last.
			for(i=1;i<=(x.length-1);i++) {
				if (x[i].indexOf(x[0]) != -1){
						System.out.printf("String %d:%s contains %s %n",i,x[i],x[0]);
						++count; }
				/**there's no else because we don't wanna do if (x[i].indexOf(x[0]) == -1)
				*It means this program won't run if there isn't checker in the messages.
				*/
			}if (count == 1) {System.out.printf("There is %d string that contains",count);
			}else{System.out.printf("There are %d strings that contain",count);}
		}
			
	}
					
}