/**This's the program that saves COE KKU student's infomations.
 * Enter infomation sample name,ID,GPA,year,parent's income .
 * and this program'll show your information 
 * and distance between your Id and the first CoE student ID.
 * Writer : Ms.Supitsara Deetoh
 * ID : 603040469-0
 * sec : 2
 Date : 18 Jan 2018*/

package deetoh.supitsara.lab2;

public class CoEKKUStudent {
	public static void main(String[] x) {
		if (x.length == 5) {
			String name = x[0]; 			//this's the first line that declare variable.
			long ID = Long.parseLong(x[1]);
			float gpa = Float.parseFloat(x[2]);
			int year = Integer.parseInt(x[3]);
			double inCome = Double.parseDouble(x[4]);
			long difFirstID = (ID - 3415717);
			
			//the first line that print CoE KKU student's information
			System.out.printf("%s has ID = %d GPA = %.2f year = %d parent's income = %.1f" 
							+ "%nYour ID is different from the first CoE student's ID by %d",
							name, ID, gpa, year, inCome, difFirstID); 
			}
		else { // if ( x.length != 5 )
			System.err.println("CoEKKUStudent <name> <ID> <GPA> <academic year> <parents' income>");
		}
	}
}
