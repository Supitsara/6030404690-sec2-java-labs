package deetoh.supitsara.lab7;
/**<h1> MySimpleWindow </h1>
 * 
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 12 March 2018 */
import java.awt.*;
import javax.swing.*;

@SuppressWarnings("serial")
class MySimpleWindow extends JFrame {
	JPanel panel;
	JButton buttonOK,buttonCancel;
	
	public MySimpleWindow(String string) {
		super(string);	
	}

	public static void createAndShowGUI() {
		
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();	
	}

	protected void setFrameFeatures() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.pack();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w)/2;
		int y = (dim.height - h)/2;
		setLocation(x,y);
	    setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	protected void addComponents() {
		panel = new JPanel(new FlowLayout());
		buttonOK = new JButton("OK");
		buttonCancel = new JButton("Cancal");
		panel.add(buttonCancel);
		panel.add(buttonOK);
	
		panel.setVisible(true);
		this.add(panel);
		
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
		public void run() {
			createAndShowGUI();}
		});
		
	}
}