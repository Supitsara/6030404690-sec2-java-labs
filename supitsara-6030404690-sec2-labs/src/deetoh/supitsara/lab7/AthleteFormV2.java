package deetoh.supitsara.lab7;
/**<h1> AthleteFormV2 </h1>
 * 
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 12 March 2018 */
import java.awt.*;

import javax.swing.*;

public class AthleteFormV2 extends AthleteFormV1 {

	public AthleteFormV2(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;
	JTextArea competTextArea;
	JRadioButton GenRadioBut;
	JPanel gender_Panel , panelM , compet_Panel ,athleteV1_Panel;
	JLabel la_Compet,la_Gender;
	JRadioButton radioBut_Male,radioBut_Female;
	ButtonGroup b1;
	
	protected void addComponents() {
		super.addComponents();
		panelM = new JPanel(new GridLayout(3,1,0,0));
		athleteV1_Panel = new JPanel(new BorderLayout());
		athleteV1_Panel.add(panel1,BorderLayout.LINE_START);
		athleteV1_Panel.add(panel2,BorderLayout.LINE_END);
		panelM.add(athleteV1_Panel);
		
		gender_Panel = new JPanel(new GridLayout(1,2));
		la_Gender = new JLabel("Gender");
		radioBut_Male =new JRadioButton("Male");
		radioBut_Female =new JRadioButton("Female");
		b1 = new ButtonGroup();
		b1.add(radioBut_Male);
		b1.add(radioBut_Female);
		gender_Panel.add(la_Gender);
		gender_Panel.add(radioBut_Male);
		gender_Panel.add(radioBut_Female);
		panelM.add(gender_Panel);////////////////////////////
		
		String story ="Competed in the 31st national championship";
		 compet_Panel = new JPanel(new GridLayout(3,1));
		 la_Compet = new JLabel("Competition");
		 competTextArea = new JTextArea(story,2,35);
		 compet_Panel.add(la_Compet,BorderLayout.AFTER_LAST_LINE);
		 compet_Panel.add(competTextArea,BorderLayout.AFTER_LAST_LINE);
		 compet_Panel.add(panel,BorderLayout.AFTER_LAST_LINE);
		panelM.add(compet_Panel,BorderLayout.AFTER_LAST_LINE);////////////////////////////
	
		this.setContentPane(panelM);
	}

	public static void createAndShowGUI(){
		AthleteFormV2 AthleteForm2 = new AthleteFormV2("Athlete Form V2");
		AthleteForm2.addComponents();
		AthleteForm2.setFrameFeatures();
		
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
