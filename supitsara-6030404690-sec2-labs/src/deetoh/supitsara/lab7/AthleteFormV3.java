package deetoh.supitsara.lab7;
/**<h1> AthleteFormV3 </h1>
 * 
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 12 March 2018 */
import java.awt.*;
import javax.swing.*;


@SuppressWarnings("serial")
public class AthleteFormV3 extends AthleteFormV2 {
	JPanel combo_Box_panel, main_Panel3, type_Panel;
	JMenuBar type;
	JLabel type_la;
	public AthleteFormV3(String title) {
		super(title);
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
		public void run() {
			createAndShowGUI();}
		});
	
		
	}
	
	///////////////////////////////////////////////////////////
	protected void addComponents() {
		super.addComponents();
		main_Panel3 = new JPanel(new BorderLayout());
			JMenuBar menu_Bar = new JMenuBar();
			JMenu File = new JMenu("File");
			JMenu Config = new JMenu("Config");
			menu_Bar.add(File);
			menu_Bar.add(Config);
			
			JMenuItem New = new JMenuItem("New");
			JMenuItem Open = new JMenuItem("Open");
			JMenuItem Save = new JMenuItem("Save");
			JMenuItem Exit = new JMenuItem("Exit");
			File.add(New);
			File.add(Open);
			File.add(Save);
			File.add(Exit);
			
			JMenuItem Color = new JMenuItem("Color");
			JMenuItem Size = new JMenuItem("Size");
			Config.add(Color);
			Config.add(Size);
			
		main_Panel3.add(menu_Bar,BorderLayout.PAGE_START);//AddPanelmenubar
		
		
			JPanel pane_1 = new JPanel(new BorderLayout());
			pane_1.add(athleteV1_Panel,BorderLayout.PAGE_START);
			pane_1.add(gender_Panel,BorderLayout.LINE_START);//Add2Panel
			pane_1.add(compet_Panel,BorderLayout.PAGE_END);
		
		main_Panel3.add(pane_1,BorderLayout.LINE_START);
			type_Panel = new JPanel(new GridLayout(1,2));
				JLabel type_La = new JLabel("Type :");
				type_Panel.add(type_La);
				String sport_Type[] = {"Badminton Player","Boxer","Footballer"};
				JComboBox type_Chooser = new JComboBox(sport_Type);
				type_Chooser.setPreferredSize(new Dimension(10,20));
				type_Panel.add(type_Chooser);
			JPanel pane_2 = new JPanel(new BorderLayout());
				pane_2.add(type_Panel,BorderLayout.PAGE_START);
				pane_2.add(panel,BorderLayout.PAGE_END);
				
		main_Panel3.add(pane_2,BorderLayout.PAGE_END);
		
		this.setContentPane(main_Panel3);
	}
	
	
	public static void createAndShowGUI(){
		AthleteFormV3 AthleteForm3 = new AthleteFormV3("Athlete Form V3");
		AthleteForm3.addComponents();
		AthleteForm3.setFrameFeatures();
		
	}
	
	protected void setFrameFeatures() {
			Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
			this.pack();
			int w = getSize().width;
			int h = getSize().height;
			int x = (dim.width - w)/2;
			int y = (dim.height - h)/2;
			setLocation(x,y);
		    setVisible(true);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		
	
}
