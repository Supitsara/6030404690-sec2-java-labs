package deetoh.supitsara.lab7;
/**<h1> AthleteFormV1 </h1>
 * 
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 12 March 2018 */
import java.awt.*;
import javax.swing.*;

public class AthleteFormV1 extends MySimpleWindow {
	private static final long serialVersionUID = 1L;
	JPanel panel1,panel2,mainpanel,lowPanel;
	JLabel label1,label2,label3,label4,label5;
	JTextField box1,box2,box3,box4,box5;
	JButton OK,cancel;
	
	public AthleteFormV1(String title) {
		super(title); }
	
    public static void createAndShowGUI(){
    	AthleteFormV1 athleteForm1 = new AthleteFormV1("Athlete Form V1");
    	athleteForm1.addComponents();
    	athleteForm1.setFrameFeatures(); }
    

    protected void addComponents() {
    	
    	mainpanel = new JPanel(new BorderLayout());
    	
    	panel1 = new JPanel(new GridLayout(5,1));
    	label1 = new JLabel("Name:");
    	label2 = new JLabel("Birthdate:");
    	label3 = new JLabel("Weight (kg.):");
    	label4 = new JLabel("Height (metre):");
    	label5 = new JLabel("Nationality:");
    	
    	panel1.add(label1);
    	panel1.add(label2);
    	panel1.add(label3);
    	panel1.add(label4);
    	panel1.add(label5);
    	
    	panel2 = new JPanel(new GridLayout(5,1));
    	box1 = new JTextField(15);
    	box2 = new JTextField(15);
    	box2.setToolTipText("ex. 22.02.200");
    	box3 = new JTextField(15);
    	box4 = new JTextField(15);
    	box5 = new JTextField(15);
    	
    	panel2.add(box1);
    	panel2.add(box2);
    	panel2.add(box3);
    	panel2.add(box4);
    	panel2.add(box5);
    	
    	super.addComponents();
    	mainpanel.add(panel1,BorderLayout.LINE_START);
    	mainpanel.add(panel2,BorderLayout.LINE_END);
    	mainpanel.add(panel,BorderLayout.PAGE_END);
    	
		this.setContentPane(mainpanel);
    }

    protected void setFrameFeatures() {
    	super.setFrameFeatures();
    }
    public static void main(String[] args) {
    	SwingUtilities.invokeLater(new Runnable() {
		public void run() {
			createAndShowGUI();
		}
	});
}
  }
