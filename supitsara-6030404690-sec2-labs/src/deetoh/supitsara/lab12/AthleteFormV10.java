package deetoh.supitsara.lab12;
/**<h1> AthleteFormV10 </h1>
 * This program call AthleteFormV10.
 * this program 's like AthleteFormV9 but more funtion
 * you can write athlete's infomation by add athlete info ,then click save to some file you want.
 * then open to read that file .
 * 
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 13 May 2018 */
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class AthleteFormV10 extends AthleteFormV9 {

	private static final long serialVersionUID = 1L;
	protected JFileChooser openJfc ,saveJfc;
	protected FileWriter fileWriter;
	protected String line, s;
	protected BufferedReader br;
	protected int result;
	private String display;
	
	public AthleteFormV10(String title) {
		super(title);
	}
	
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();
		super.actionPerformed(e);
		
		if(src == open) {
			handleOpenItem();	
		}
		else if(src == save) {
			handleSaveItem();
		}	
	}
	
	protected void handleOpenItem() {
		openJfc = new JFileChooser();
		openJfc.setDialogType(JFileChooser.OPEN_DIALOG);
		result = openJfc.showOpenDialog(null);
		
		if (result == JFileChooser.APPROVE_OPTION) {
				try {
					File fileselected = saveJfc.getSelectedFile();
					new BufferedReader(new FileReader(fileselected.getPath()));
					line = "";
					s = "";
					while ((line = br.readLine()) != null) {
						s += (line+"\n");
					}
					JOptionPane.showMessageDialog(null, s);
					if (br != null) {
						br.close();
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(this, "Something happen! please try again.", "Warning", JOptionPane.ERROR_MESSAGE);
				}
		} else {
			JOptionPane.showMessageDialog(this, "Opening command cancelled by user.");
		}
	}

		protected void handleSaveItem() {
			saveJfc = new JFileChooser();
			saveJfc.setDialogType(JFileChooser.SAVE_DIALOG);
			
			int result = saveJfc.showSaveDialog(null);
		
			if (result == JFileChooser.APPROVE_OPTION) {
					File selected = saveJfc.getSelectedFile();
					try {
						fileWriter = new FileWriter(selected.getPath());
						display = "";
						for (int i = 0; i < arrText.size() ; i++) {
							display += (i+1) + ". " + arrText.get(i) + "\n";
						}
						fileWriter.write(display);
						fileWriter.flush();
					} catch (Exception e) {
						JOptionPane.showMessageDialog(this, "Something happen! please try again.", "Warning", JOptionPane.ERROR_MESSAGE);
					}
					JOptionPane.showMessageDialog(this, "Saving : " + selected.getName() + "");
			} else {
				JOptionPane.showMessageDialog(this, "Saving command cancelled by user.");
			}
		}
	
	public void addListeners() {
		super.addListeners();
		open.addActionListener(this);
		open.removeActionListener(open.getActionListeners()[0]);
		save.addActionListener(this);
		save.removeActionListener(open.getActionListeners()[0]);
	}
	public static void createAndShowGUI(){
		AthleteFormV10 AthleteV10 = new AthleteFormV10("Athlete Form V10");
		AthleteV10.addComponents();
		AthleteV10.setFrameFeatures();
		AthleteV10.addListeners();
	}
	public static void main(String[] arg) {
		SwingUtilities.invokeLater(new Runnable() {	
			public void run() {
				createAndShowGUI();	
			}
		});
	}
	
	

}
