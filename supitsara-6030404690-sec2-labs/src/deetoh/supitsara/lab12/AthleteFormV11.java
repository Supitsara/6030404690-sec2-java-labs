package deetoh.supitsara.lab12;
/**<h1> AthleteFormV11 </h1>
 * This program call AthleteFormV11.
 * When you add athlete's info and it haven't complete yet.'it 'll be shown dialog with message to tell you that it'doesn't complete.
 * such as if you doesn't add athlete's name ; it'll show "Please enter athlete name".
 * and if you doesn't add name,weigth,heigth,datetime and nationality (5 textfield)
 *  it'll show 5 dialog.with the message.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 13 May 2018 */
import java.time.format.DateTimeParseException;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class AthleteFormV11 extends AthleteFormV10 {

	
	private static final long serialVersionUID = 1L;

	public AthleteFormV11(String title) {
		super(title);
	}
	
	public void handleOKbutton() {
		try {
			if (box1.getText().isEmpty() || box1.getText().matches("^\\d+")) {
				throw new NoNameException() ;
			}
		} catch(NoNameException e){
			String msg = "Please enter athlete name";
			JOptionPane.showMessageDialog(this, msg);
		}
		
		try {
			if (box2.getText().isEmpty() || !box2.getText().matches("\\d{2}/\\d{2}/\\d{4}")) {
				throw new DateTimeParseException("Please enter date in the format dd/MM/yyyy  such as 02/02/2000", box2.getText(), 0) ;
			}
		} catch(DateTimeParseException e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		
		try {
			if (box3.getText().isEmpty() || !box3.getText().matches("[0-9]+")) {
				throw new NumberFormatException() ;
			}
		} catch(NumberFormatException e){
			String msg = "Please enter athlete weigth";
			JOptionPane.showMessageDialog(this, msg);
		}
		
		try {
			if (box4.getText().isEmpty() || !box4.getText().matches("[0-9]+")) {
				throw new NumberFormatException() ;
			}
		} catch(NumberFormatException e){
			String msg = "Please enter athlete heigth";
			JOptionPane.showMessageDialog(this, msg);
		}
		
		try {
			if (box5.getText().isEmpty() || box5.getText().matches("^\\d+")) {
				throw new NoNameException() ;
			}
		} catch(NoNameException e){
			String msg = "Please enter athlete nationality";
			JOptionPane.showMessageDialog(this, msg);
		}
	}
	
	
	public static void createAndShowGUI(){
		AthleteFormV11 AthleteV11 = new AthleteFormV11("Athlete Form V11");
		AthleteV11.addComponents();
		AthleteV11.setFrameFeatures();
		AthleteV11.addListeners();
	}
	public static void main(String[] arg) {
		SwingUtilities.invokeLater(new Runnable() {	
			public void run() {
				createAndShowGUI();	
			}
		});
	}
	
	

}
