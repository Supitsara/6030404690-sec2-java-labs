package deetoh.supitsara.lab12;
/**<h1> AthleteFormV9 </h1>
 * This program call AthleteFormV9.
 * This program adds new function.you can clicks search and then it's show dialog to input name athlete. 
 * but it has to athlete's infomation in,it's will show nothing
 * When you click remove if it has athlete's name in ,then that name 'll be removed.
 * but if there're no that name ,then it's show dialog "(filename) is not found".
 * 
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 13 May 2018 */
import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import deetoh.supitsara.lab11.AthleteFormV8;

public class AthleteFormV9 extends AthleteFormV8 {

	protected String inputname,removename;
	protected String msg,msgRemove;
	private static final long serialVersionUID = 1L;

	public AthleteFormV9(String title) {
		super(title);
	}
	
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();
		super.actionPerformed(e);
		if( src == search) {
			inputname = JOptionPane.showInputDialog("Please enter the athlete name");
			search();
		}
		else if( src == remove) {
			remove();
		}
			
	}
	
	public void search() {
		
		for( int i=0 ; i<arrText.size() ; i++ ) {
			
			if( inputname.equals(arrText.get(i).getName())) {
				msg = arrText.get(i).toString();
				JOptionPane.showMessageDialog(this,msg);
				break;
			}
			else {
				msg = inputname+" is not found";
				JOptionPane.showMessageDialog(this,msg);
			}
		}
		
	}
	
	public void remove() {
		removename = JOptionPane.showInputDialog("Please enter the athlete name to remove");
		
		if( arrText.size() == 0) {
			JOptionPane.showMessageDialog(this,removename+" is not found");
		}	
		else { for( int i=0 ; i<arrText.size() ; i++ ) {
			if( removename.equals(arrText.get(i).getName())) {
				msgRemove = arrText.get(i).toString();
				arrText.remove(arrText.get(i));
				JOptionPane.showMessageDialog(this,msgRemove);
			} else {
				msgRemove = removename+" is not found";
				JOptionPane.showMessageDialog(this,msgRemove);
			}
		}}
	}
	
	public void addListeners() {
		super.addListeners();
		search.addActionListener(this);
		remove.addActionListener(this);
	}
	public static void createAndShowGUI(){
		AthleteFormV9 AthleteV9 = new AthleteFormV9("Athlete Form V9");
		AthleteV9.addComponents();
		AthleteV9.setFrameFeatures();
		AthleteV9.addListeners();
	}
	public static void main(String[] arg) {
		SwingUtilities.invokeLater(new Runnable() {	
			public void run() {
				createAndShowGUI();	
			}
		});
	}
}
