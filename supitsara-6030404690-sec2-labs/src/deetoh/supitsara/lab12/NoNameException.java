package deetoh.supitsara.lab12;
/**<h1> emptyException </h1>
 * This program call emptyException.
 * 
 * 
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 13 May 2018 
 */

public class NoNameException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public NoNameException() {}
	
	public NoNameException(String name) {
		super(name);
	}

}
