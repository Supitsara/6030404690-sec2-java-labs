package deetoh.supitsara.lab10;
/**<h1> PongGameV1 </h1>
 * This program is called PongGameV1
 * It'll show a drawing a white ball ,line ,text score.
 * it has a black background.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 7/4/2018 */
import javax.swing.SwingUtilities;


public class PongGameV1 extends SimpleGameWindow{
	
	private static final long serialVersionUID = 1L;

	public PongGameV1(String string) {
		super(string);
	}

	public static void createAndShowGUI() {
		PongGameV1 window = new PongGameV1("CoE Pong Game V1");
		window.addComponents();
		window.setFrameFeatures();
	}
	
	private void addComponents() {
		setContentPane(new PongGamePanel());
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}