package deetoh.supitsara.lab10;
/**<h1> PongPaddle </h1>
 * This program is called PongPaddle 
 * That's for building paddle ,how it's located or resized.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 7/4/2018 */
import java.awt.geom.Rectangle2D;

public class PongPaddle extends Rectangle2D.Double{
	
	private static final long serialVersionUID = -3420859037732960535L;
	public static int HEIGHT = 80;
	public static int WIDTH = 15;
	
	public PongPaddle(int x, int y, int w, int h) {
		super(x,y,w,h);
	}

	public int getW() {
		return (int) this.getWidth();
	}


	public int getH() {
		return (int) this.getHeight();
	}

}
