package deetoh.supitsara.lab10;
/**<h1> PongGameV1 </h1>
 * This program is called PongGameV1
 * It'll show a drawing a white ball ,line ,text score.
 * it has a black background.
 * It's show a moving ball that can bounced by two paddles or up and low frame
 * and it's set to center when it's out of frame.
 * Paddles 're controlled by key(S,W,Up and Down)
 * 
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 7/4/2018 */
import javax.swing.SwingUtilities;

import deetoh.supitsara.lab10.SimpleGameWindow;

public class PongGameV3 extends SimpleGameWindow {

	private static final long serialVersionUID = 1L;
	public PongGameV3(String title) {
		super(title);
	}
	
	public static void createAndShowGUI() {
		PongGameV3 window = new PongGameV3("CoE Pong Game V3");
		window.addComponents();
		window.setFrameFeatures();
	}

	private void addComponents() {
		setContentPane(new MovingPongGamePanelV2());
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}