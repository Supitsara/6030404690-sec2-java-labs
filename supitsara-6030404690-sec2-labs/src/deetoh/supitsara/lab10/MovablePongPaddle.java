package deetoh.supitsara.lab10;
/**<h1> MovablePongPaddle </h1>
 * This program is called MovablePongPaddle 
 * That's for building Paddle ,how it's located.
 * Paddles have a velocity and vector.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 7/4/2018 */
import deetoh.supitsara.lab10.PongPaddle;
import deetoh.supitsara.lab10.SimpleGameWindow;

public class MovablePongPaddle extends PongPaddle{
	
	private static final long serialVersionUID = 1L;
	private int velocityUp;
	private int velocityDown;
	private int PAD_SPEED = 10;
	
	public MovablePongPaddle(int x, int y, int w, int h) {
		super(x, y, w, h);
	}

	public int getVelocityUp() {
		return velocityUp;
	}

	public void setVelocityUp(int velocityUp) {
		this.velocityUp = velocityUp;
	}

	public int getVelocityDown() {
		return velocityDown;
	}

	public void setVelocityDown(int velocityDown) {
		this.velocityDown = velocityDown;
	}
	
	public void moveUp(){
		if(this.y - PAD_SPEED >= 0)
			this.y-=PAD_SPEED;
	}
	
	public void moveDown() {
		if(this.y + this.height+ PAD_SPEED <= SimpleGameWindow.HEIGHT)
			this.y+=PAD_SPEED;
	}
	
}