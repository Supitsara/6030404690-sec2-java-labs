package deetoh.supitsara.lab10;
/**<h1> Ball </h1>
 * This program is called Ball 
 * That's for building ball ,how it's located.
 * It's a ball that don't have a velocity
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 7/4/2018 */
import java.awt.geom.Ellipse2D;

public class Ball extends Ellipse2D.Double{
	
	private static final long serialVersionUID = 150974525667001681L;
	
	private int r;
	
	public Ball(int x, int y, int r) {
		super(x, y, 2*r, 2*r);
		this.r = r;
	}

	public int getR() {
		return r;
	}

	public void setR(int r) {
		this.r = r;
	}
	
}