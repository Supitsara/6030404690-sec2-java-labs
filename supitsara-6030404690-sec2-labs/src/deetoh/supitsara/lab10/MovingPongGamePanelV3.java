package deetoh.supitsara.lab10;
/**<h1> MovingPongGamePanelV3 </h1>
 * This program is called MovingPongGamePanelV3
 * It's defined by drawing a ball,line,color
 * and controling by key press S ,W ,Up ,Down.
 * Paddles have a velocity and vector.
 * Ball can run and be touched by two paddles;
 * when it's almost out of frame(black area) It'll bounce to another vector.
 * and calculated scores when opposited side can't bounce the moving ball.
 * until scores is 10 ,we 'll have a winner then end the game.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 7/4/2018 */
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.util.Random;

import javax.swing.JPanel;

import deetoh.supitsara.lab10.PongPaddle;
import deetoh.supitsara.lab10.SimpleGameWindow;
import deetoh.supitsara.lab10.MovablePongPaddle;
import deetoh.supitsara.lab10.MovingBall;

public class MovingPongGamePanelV3 extends JPanel implements Runnable, KeyListener {

	protected MovingBall movingBall;
	private Thread running;
	private Random rand;
	private int ballR = 20;
	protected MovablePongPaddle movableRightPad;
	protected MovablePongPaddle movableLeftPad;
	protected Rectangle2D.Double box;
	protected Integer player1Score;
	protected Integer player2Score;

	public MovingPongGamePanelV3() {
		super();

		addKeyListener(this);
		setFocusable(true);

		setBackground(Color.BLACK);



		// initialize the pads
		movableLeftPad = new MovablePongPaddle(0, SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH,
				PongPaddle.HEIGHT);
		movableRightPad = new MovablePongPaddle(SimpleGameWindow.WIDTH - PongPaddle.WIDTH,
				SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH, PongPaddle.HEIGHT);
		resetBall();
	
		// initialize the ball
		box = new Rectangle2D.Double(0, 0, SimpleGameWindow.WIDTH, SimpleGameWindow.HEIGHT);

		// set the player scores
		player1Score = 0;
		player2Score = 0;

		running = new Thread(this);
		running.start();
	}

	private void resetBall() {
		Random rand = new Random();
		int  n = rand.nextInt(7) - 3;
		int  m = rand.nextInt(7) - 3;
		if(n != 0 ) {
			movingBall = new MovingBall(SimpleGameWindow.WIDTH/2 - ballR,SimpleGameWindow.HEIGHT/2,20,n,m);
		}
	}

	@Override
	public void run() {
		
		while (true) {

			moveBall();
			repaint();
			if (player1Score == 10||player2Score==10) {
				break;
			}
			this.getToolkit().sync(); // to flush the graphic buffer
			
			// Delay
			try {
				// try to adjust the number here to have a smooth
				// running ball on your machine
				Thread.sleep(15);
			} catch (InterruptedException ex) {
				System.err.println(ex.getStackTrace());
			}
			
		}
	}

	// update position of the ball
	
	private void moveBall() {
		movingBall.move();
		if(movingBall.getY() == SimpleGameWindow.HEIGHT -40 || movingBall.getY()== 0) {
			movingBall.setVelY((-1)*movingBall.getVelY());
		}
		//right paddle
		if(movingBall.chkMoveX() + movingBall.width   >  movableRightPad.x && 
				movingBall.chkMoveY() + movingBall.height  >= movableRightPad.y &&
				movingBall.chkMoveY() <= movableRightPad.y +  movableRightPad.height ) 
		{
			movingBall.setVelX((-1)*movingBall.getVelX());
		}
		//right paddle
		if(movingBall.chkMoveX() - movableLeftPad.width  < movableLeftPad.x && 
				movingBall.chkMoveY() + movingBall.height >= movableLeftPad.y && 
				movingBall.chkMoveY() <= movableLeftPad.y + movableLeftPad.height)
		{	
			movingBall.setVelX((-1)*movingBall.getVelX());
		}
			if (movingBall.x > SimpleGameWindow.WIDTH+50) {
				player1Score+=1;
				resetBall();	
			}
			//Left
			if(movingBall.x < -50) {
				player2Score+=1;
				resetBall();
			}	
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g;

		g2.setColor(Color.WHITE);

		// draw the middle line
		g2.drawLine(SimpleGameWindow.WIDTH / 2, 0, SimpleGameWindow.WIDTH / 2, SimpleGameWindow.HEIGHT);

		// draw line on the left
		g2.drawLine(movableLeftPad.getW(), 0, movableLeftPad.getW(), SimpleGameWindow.HEIGHT);

		// draw line on the right
		g2.drawLine(SimpleGameWindow.WIDTH - movableRightPad.getW(), 0, SimpleGameWindow.WIDTH- movableRightPad.getW(),
				SimpleGameWindow.HEIGHT);

		// Draw the score
		g2.setFont(new Font(Font.SERIF, Font.BOLD, 48));
		g2.drawString(player1Score.toString(), SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT / 5);
		g2.drawString(player2Score.toString(), 3 * SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT / 5);
		// Draw Who wins
		if(player1Score == 10) {
			g2.drawString("Player 1 wins.", SimpleGameWindow.WIDTH / 2 - 150, SimpleGameWindow.HEIGHT / 2);
			
		}
		else if(player2Score == 10) {
			g2.drawString("Player 2 wins.", SimpleGameWindow.WIDTH / 2 - 150, SimpleGameWindow.HEIGHT / 2);
		}
		
		// Draw the paddles
		g2.fill(movableLeftPad);
		g2.fill(movableRightPad);

		// draw the ball
		g2.fill(movingBall);

		// draw the box
		g2.draw(box);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int src = e.getKeyCode();
		if (src == KeyEvent.VK_W) {
			movableLeftPad.moveUp();
		}
		else if(src == KeyEvent.VK_UP) {
			movableRightPad.moveUp();
		}
		else if(src == KeyEvent.VK_S) {
			movableLeftPad.moveDown();;
		}
		else if(src == KeyEvent.VK_DOWN) {
			movableRightPad.moveDown();
		}
		repaint();
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		
	}
}
