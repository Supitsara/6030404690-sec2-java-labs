package deetoh.supitsara.lab10;
/**<h1> PongGameV4 </h1>
 * This program is called PongGameV4
 * It'll show a drawing a ball,line,color
 * and controling paddle by key press S ,W ,Up ,Down.
 * Paddles have a velocity and vector.
 * Ball can run and be touched by two paddles;
 * when it's almost out of frame(black area) It'll bounce to another vector.
 * and calculate scores when opposited side can't bounce the moving ball.
 * until scores is 10 ,we 'll have a winner then end the game.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 7/4/2018 */
import javax.swing.SwingUtilities;

import deetoh.supitsara.lab10.SimpleGameWindow;

public class PongGameV4 extends SimpleGameWindow{
	
	private static final long serialVersionUID = 1L;

	public PongGameV4(String title) {
		super(title);
	}
	
	public static void createAndShowGUI() {
		PongGameV4 window = new PongGameV4("CoE Pong Game V4");
		window.addComponents();
		window.setFrameFeatures();
	}

	private void addComponents() {
		setContentPane(new MovingPongGamePanelV3());
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
