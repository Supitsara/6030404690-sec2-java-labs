package deetoh.supitsara.lab10;
/**<h1> PongGameV2 </h1>
 * This program is called PongGameV2
 * It'll show a drawing a white ball ,line ,text score.
 * it has a black background.
 * Stopped ball 's at the center.
 * two Paddles can controlled by key(S,W,Up,Down).
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 7/4/2018 */
import javax.swing.SwingUtilities;
import deetoh.supitsara.lab10.SimpleGameWindow;

public class PongGameV2 extends SimpleGameWindow {

	private static final long serialVersionUID = 1L;

	public PongGameV2(String title) {
		super(title);
	}
	
	public static void createAndShowGUI() {
		PongGameV2 window = new PongGameV2("CoE Pong Game V2");
		window.addComponents();
		window.setFrameFeatures();
	}

	private void addComponents() {
		setContentPane(new MovingPongGamePanel());
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
