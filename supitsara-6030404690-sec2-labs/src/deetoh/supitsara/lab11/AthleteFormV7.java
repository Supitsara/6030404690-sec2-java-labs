package deetoh.supitsara.lab11;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import javax.swing.*;

import deetoh.supitsara.lab9.AthleteFormV6;
/**<h1> AthleteFormV7 </h1>
 * This program is called AthleteFormV7
 * It's like AthleteFormV6 but more funtional.
 * when you choose config color, then you can chage font's color and text area.
 * blue, red, green color 'll change font and text area.
 * and others 'll change just text area.
 * morever, when you enter athlete's info then click OK 
 * It'll show athlete's info in the dialog.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 27/4/2018 */
public class AthleteFormV7 extends AthleteFormV6 {
	private static final long serialVersionUID = 1L;
	protected JPanel mainPanelV7;
	protected JMenuBar jmbV7;
	protected JMenu file,config;
	
	public AthleteFormV7(String title) {
		super(title);
	}

	@Override
	public void addComponents() {
		super.addComponents();
		
		mainPanelV7 = new JPanel( new BorderLayout() );
		jmbV7 = new JMenuBar();
		file = new JMenu("file");
		file.setMnemonic(KeyEvent.VK_F);//must br Alt + F
		New = new JMenuItem("New");
		New.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
		open = new JMenuItem("Open",new ImageIcon("images/openIcon.png"));
		open.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		save = new JMenuItem("Save",new ImageIcon("images/saveIcon.png"));
		save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		exit = new JMenuItem("Exit",new ImageIcon("images/quitIcon.png"));
		exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK));

		config = new JMenu("Config");
		color = new JMenu("Colors");
		size = new JMenu("Size");
		
		//////////////////////
		blue = new JMenuItem("Blue");
		green = new JMenuItem("Green");
		red = new JMenuItem("Red");
		custom = new JMenuItem("Custom...");//use again
		
		color.add(blue);
		color.add(green);
		color.add(red);
		color.add(custom);
		config.add(color);
		//Add color menu:16,20,24,custom... 
		s16 = new JMenuItem("16");
		s20 = new JMenuItem("20");
		s24 = new JMenuItem("24");
		size.add(s16);
		size.add(s20);
		size.add(s24);
		//////////////////////
		
		config.add(color);
		config.add(size);
		
		
		
		file.add(New);
		file.add(open);
		file.add(save);
		file.add(exit);
		
		jmbV7.add(file);
		jmbV7.add(config);
		
		mainPanelV7.add(jmbV7,BorderLayout.PAGE_START);
		mainPanelV7.add(pic,BorderLayout.CENTER);
		mainPanelV7.add(subPanelV5,BorderLayout.PAGE_END);
		setContentPane(mainPanelV7);
	}
	public void addListeners() {
		super.addListeners();
		New.addActionListener(this);
		open.addActionListener(this);
		save.addActionListener(this);
		exit.addActionListener(this);
		blue.addActionListener(this);
		green.addActionListener(this);
		red.addActionListener(this);
		s16.addActionListener(this);
		s20.addActionListener(this);
		s24.addActionListener(this);
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		
		Object src = e.getSource();
		JFileChooser fc = new JFileChooser();
			
			if (src == open ) {
				int retVal = fc.showOpenDialog(this);
				
				if (retVal == JFileChooser.APPROVE_OPTION ) {
					File fileSelected = fc.getSelectedFile();
					JOptionPane.showMessageDialog(this, "Opening:"+fileSelected.getName());
				}
				else if (retVal == JFileChooser.CANCEL_OPTION ) {
					JOptionPane.showMessageDialog(this, "Open comand cancelled by user.");
				}
			
			}
			else if (src == save) {
				int retVal1 = fc.showOpenDialog(this);
			
				if (retVal1 == JFileChooser.APPROVE_OPTION ) {
					File fileSelected = fc.getSelectedFile();
					JOptionPane.showMessageDialog(this, "Saving:"+fileSelected.getName());
				}
				else if (retVal1 == JFileChooser.CANCEL_OPTION ){
					JOptionPane.showMessageDialog(this, "Saving comand cancelled by user.");
				}
				
			}
			else if (src == exit) {
				System.exit(0);
			}
			
			if(src == blue) {
				getColor(Color.blue);
			}
			else if (src == green) {
				getColor(Color.green);
			}
			else if (src == red) {
				getColor(Color.red);
			}
			else if (src == custom) {
				  Color initialBackground = competTextArea.getBackground();
			      Color background = JColorChooser.showDialog(null, "Choose colors",
			            initialBackground);
			        if (background != null) {
			        	competTextArea.setBackground(background);
			        }
			}
			
			if (src == s16) {
				setSize(16);
			}
			else if (src == s20) {
				setSize(20);
			}
			else if (src == s24) {
				setSize(24);
			}
		
	}
	public void getColor(Color paint) {
		box1.setForeground(paint);
		box2.setForeground(paint);
		box3.setForeground(paint);
		box4.setForeground(paint);
		box5.setForeground(paint);	
		competTextArea.setBackground(paint);
	}
	
	public static void main(String[] arg) {
		SwingUtilities.invokeLater(new Runnable() {	
			public void run() {
				createAndShowGUI();	
			}
		});
	}
	
	public static void createAndShowGUI(){
		AthleteFormV7 AthleteV7 = new AthleteFormV7("Athlete Form V7");
		AthleteV7.addComponents();
		AthleteV7.setFrameFeatures();
		AthleteV7.addListeners();
	}
	
}
