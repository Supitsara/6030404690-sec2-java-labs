package deetoh.supitsara.lab11;

import java.awt.event.ActionEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import deetoh.supitsara.lab5.Athlete;
import deetoh.supitsara.lab5.Gender;
/**<h1> AthleteFormV8 </h1>
 * This program is called AthleteFormV8
 * It's like AthleteFormV7 but more funtional.
 * 4 new objectss that's added are display,sort,search,remove.
 * display : that'll show rank and athlete's info.
 * sort : that's sorted by athlete's height.
 * search and remove : It've not controlled.
 * 
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 27/4/2018 */
public class AthleteFormV8 extends AthleteFormV7 {
	
	private static final long serialVersionUID = 1L;
	protected ArrayList<Athlete> arrText,space;
	protected String text = "", message = "", message2 = "";
	protected Athlete athletes;
	protected JMenu data;
	protected JMenuItem disPlay,sort,search,remove;
	protected String type;
	
	protected String name;
	protected double weight;
	protected double height;
	protected Gender gender;
	protected String nationality;
	protected String birthdate;
	

	public AthleteFormV8(String title) {
		super(title);
		arrText = new ArrayList<Athlete>();
		
	}
	
	@Override
	public void addComponents() {
		super.addComponents();
		data = new JMenu("Data");
		disPlay = new JMenuItem("Display");
		search = new JMenuItem("Search");
		sort = new JMenuItem("Sort");
		remove = new JMenuItem("Remove");
		
		data.add(disPlay);
		data.add(search);
		data.add(sort);
		data.add(remove);
		jmbV7.add(data);
		
	}
	
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		Object src = e.getSource();
		
			type = String.valueOf(type_Chooser.getSelectedItem());
			
			text = " Name: "+box1.getText()+", Birthdate: = "+box2.getText()+", Weight = "
						   +box3.getText()+", Height = "+box4.getText()+", Nationality = "
						   +box5.getText()+"\nGender = "+getGender()+"\n Compettition = "+competTextArea.getText()+"\nType = "+type;
		
			
			if(src == OK) {
				addAthlete();
			}
			else if(src == disPlay) {
				displayAthletes();
			}
			else if(src == sort) {
				Collections.sort(arrText, new HeightComparator());
				displayAthletes();
			}
	}

	public void displayAthletes(){
		
		message = "";
		for(int i=0 ; i<arrText.size() ; i++) {
			message += Integer.toString(i+1)+": "+arrText.get(i)+"\n";
		}
		JOptionPane.showMessageDialog(this,message);
	}
	
	public void addAthlete() {
		name = box1.getText();
		birthdate = box2.getText();
		weight = Double.parseDouble(box3.getText());
		height = Double.parseDouble(box4.getText());
		nationality = box5.getText();
		type = String.valueOf(type_Chooser.getSelectedItem());
		
		if(radioBut_Male.isSelected()) {
			gender = Gender.MALE;
		}else {
			gender = Gender.FEMALE;}
		
		athletes = new Athlete(name, weight, height, gender, nationality, birthdate);
		arrText.add(athletes);
		
		message = "";
		for(int i=0 ; i<arrText.size() ; i++) {
			
			message += arrText.get(i)+"\n";	
		}
		JOptionPane.showMessageDialog(frame,message);
		System.out.println(arrText);
	}

	public void addListeners() {
		super.addListeners();
		disPlay.addActionListener(this);
		sort.addActionListener(this);
	}
	
	public static void createAndShowGUI(){
		AthleteFormV8 AthleteV8 = new AthleteFormV8("Athlete Form V8");
		AthleteV8.addComponents();
		AthleteV8.setFrameFeatures();
		AthleteV8.addListeners();
	}

	public static void main(String[] arg) {
		SwingUtilities.invokeLater(new Runnable() {	
			public void run() {
				createAndShowGUI();	
			}
		});
	}

}
