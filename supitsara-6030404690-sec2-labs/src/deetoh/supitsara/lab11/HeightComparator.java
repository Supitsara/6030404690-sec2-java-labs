package deetoh.supitsara.lab11;
/**<h1> HeightComparator </h1>
 * This program is called HeightComparator
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 27/4/2018 */
import java.util.Comparator;
import deetoh.supitsara.lab5.Athlete;

public class HeightComparator implements Comparator<Athlete> {

	public int compare(Athlete a, Athlete b) {
		return a.getHeight() < b.getHeight() ? -1 : a.getHeight() == b.getHeight() ? 0 : 1;
	}
	
}
