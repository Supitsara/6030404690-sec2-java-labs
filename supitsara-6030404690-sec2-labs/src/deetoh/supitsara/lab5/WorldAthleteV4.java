package deetoh.supitsara.lab5;
/**<h1> WorldAthleteV4 </h1>
 * this class built 3 obj such as ratchanok, tom and wisaksil.
 * and show how those method behavior.
 * <p>
 * @author	Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 2.0
 * @since	9Feb2018*/
public class WorldAthleteV4 implements Playable,Movable {
	public static void main(String[] args) {
	    BadmintonPlayer ratchanok = new BadmintonPlayer("Ratchanok Intanon", 
	            55, 1.68, Gender.FEMALE, "Thai", "05/02/1995", 66.5,  4);
	    Footballer tom = new Footballer("Tom Brady", 102, 1.93, Gender.MALE, 
	            "American", "03/08/1977", "Quarterback",  "New England Patriots");
	    Boxer wisaksil = new Boxer("Wisaksil Wangek", 51.5, 1.60, Gender.MALE,
	            "Thai", "08/12/1986", "Super Flyweight", "M" );
	    
	    ratchanok.play();
	    tom.play();
	    wisaksil.play();
	    
	    ratchanok.move();;
	    tom.move();
	    wisaksil.move();
	}

	@Override
	public void move() {
	}

	@Override
	public void play() {
	}

}
