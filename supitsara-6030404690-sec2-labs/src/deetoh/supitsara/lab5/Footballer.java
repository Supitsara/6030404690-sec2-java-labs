package deetoh.supitsara.lab5;

/**<h1> Footballer </h1>
 * This program is called Boxer
 * This program will be get Footballer's infomation sample name ,weight(kg) ,height(m) ,gender(must choose MALE or FEMALE)
 * , nationality , birthdate(dd/MM/yyyy),position and team then enter information
 * <p>
 * @author Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 3 Jan 2018 */
	
public class Footballer extends Athlete {
	static String sport = "American Football";
	private String position;
	private String team;
	
	/**
	 * @param name Firstname and lastname
	 * @param weight Weight in kilograms
	 * @param height Height in meters
	 * @param gender Using enum with values as MALE or FEMALE
	 * @param nationality Full nationality name, such as Thai
	 * @param birthdate Date of birth in format 31/12/2017
	 * @param position position of team
	 * @param team team name
	 */
		
	public Footballer(String name, double weight, double height, Gender gender, String nationality, String birthdate,String position,String team) {
		super(name, weight, height, gender, nationality, birthdate);
		this.position = position;
		this.team = team;
	}
	
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public static String getSport() {
		return sport;
	}
	public static void setSport(String sport) {
		Footballer.sport = sport;
	}
	@Override
	public void playSport() {
		System.out.println(getName()+" is goot at "+Footballer.getSport());
	}


	@Override
	public String toString() {
		return getName()+", "+getWeight()+"kg, "+getHeight()+"m, "+getGender()+", "+getNationality()+", "+getBirthdate()+", "+sport+", "+position+", "+team ;
	}
	
	@Override
	public void play() {
		System.out.println(getName()+" throws a touchdown.");
		
	}
	@Override
	public void move() {
		System.out.println(getName()+" moves down the football field");
		
	}
		
}
