package deetoh.supitsara.lab5;
/**<h1> Competition </h1>
 * there're 4 protect variables such as name ,date ,place and description 
 * then getter and setter 4 variables.
 * <p>
 * @author Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 9 Feb 2018 */
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

abstract class Competition {
	protected String name;
	protected LocalDate date;
	protected String place;
	protected String description;
	
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");


	public DateTimeFormatter getFormatter() {
		return formatter;
	}

	public void setFormatter(DateTimeFormatter formatter) {
		this.formatter = formatter;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = LocalDate.parse(date);
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}	

}
