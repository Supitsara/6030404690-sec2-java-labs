package deetoh.supitsara.lab5;

/**<h1> Athlete </h1>
 * This program is called Athlete
 * This program will be get athlete's infomation sample name ,weight(kg) ,height(m) ,gender(must choose MALE or FEMALE)
 * , nationality and birthdate(dd/MM/yyyy) then enter information ,the program'll show info that athlete enter in the form.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 29 Jan 2018 */

import java.time.LocalDate;
import java.time.format.*;
public class Athlete {
	
	private String name;
	private double weight;
	private double height;
	private Gender gender; 
	private String nationality;
	private LocalDate birthdate;
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	
	/**@param name Firstname and lastname
	 * @param weight Weight in kilograms
	 * @param height Height in meters
	 * @param gender Using enum with values as MALE or FEMALE
	 * @param nationality Full nationality name, such as Thai
	 * @param birthdate Date of birth in format 31/12/2017
	 */
	public Athlete(String name, double weight, double height, Gender gender, String nationality, String birthdate) {
		super();
		this.name = name;
		this.weight = weight;
		this.height = height;
		this.gender = gender;
		this.nationality = nationality;	
		this.birthdate = LocalDate.parse(birthdate,formatter);
	}
		
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public LocalDate getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}
	public void playSport() {
		System.out.println( getName() +" is goot at sport.");
	}
	
	
	public String toString() {
		return "Athete [" + name + ", " + weight + "kg, " + height + "m, " + gender
				+ ", " + nationality + ", " + birthdate + "]";
	}

	public void play() {
		System.out.println("");
		
	}
	public void move() {
		System.out.println("");
	}
		
	
}
