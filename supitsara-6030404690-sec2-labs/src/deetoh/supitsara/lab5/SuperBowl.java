package deetoh.supitsara.lab5;
/**<h1> SuperBowl </h1>
 * there're 3 new variables such as AFCTeam ,NFCTeamand and winningTeam ,
 * getter and setter new variables,
 * setDescriptionAndRules method do different from this one from ThailandTournament class,
 * 2 constructors do different fuction.
 * toString method that returns text.
 * <p>
 * @author Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 9 Feb 2018 */
import java.time.LocalDate;

public class SuperBowl extends Competition {
	public String AFCTeam;
	public String NFCTeam;
	public String winningTeam;
	
	void setDescriptionAndRules() {
		System.out.println("===== Begin: Description and Rules ================");
		System.out.println("SuperBowlLII is played the champions of the National Football Conference (NFC)");
		System.out.println("and the American Football Conference (AFC).");
		System.out.println("the game plays in four quarters while each quarter takes about 15 minutes.");
		System.out.println("===== End: Description and Rules ================");
	}
	
	

	public String getAFCTeam() {
		return AFCTeam;
	}
	public void setAFCTeam(String aFCTeam) {
		AFCTeam = aFCTeam;
	}
	public String getNFCTeam() {
		return NFCTeam;
	}
	public void setNFCTeam(String nFCTeam) {
		NFCTeam = nFCTeam;
	}
	public String getWinningTeam() {
		return winningTeam;
	}
	public void setWinningTeam(String winningTeam) {
		this.winningTeam = winningTeam;
	}
	
	public SuperBowl(String name, String date, String place, String description) {
		this.name = name;
		this.place = place;
		this.description = description;
		this.date = LocalDate.parse(date,formatter);
	}

	public SuperBowl(String name, String date, String place, String description,
			String aFCTeam, String nFCTeam, String winningTeam) {
		this.AFCTeam = aFCTeam;
		this.NFCTeam = nFCTeam;
		this.winningTeam = winningTeam;
		this.name = name;
		this.place = place;
		this.description = description;
		this.date = LocalDate.parse(date,formatter);
	}



	@Override
	public String toString() {
		return name+"("+description+")"+" was held on "+date+" in "+place+"\n"
						+" It was the game between "+AFCTeam+" vs. "+NFCTeam+"\n"
						+" the winner was "+winningTeam;
	}
	

}
