package deetoh.supitsara.lab5;
/** @author Ms.Supitsara Deetoh  603040469-0  sec2
* @version 1.0
* @since 9 Feb 2018 */
interface Playable {
	void play();
}