package deetoh.supitsara.lab5;
/**<h1> SuperBowl </h1>
 * this class built 3 obj such as superBowlII, superBowlLI and the20thGoOpen.
 * and show how that method do.
 * <p>
 * @author Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 9 Feb 2018 */
public class InterestingCompetitions {
	public static void main(String[] args) {
		SuperBowl superBowlII = new SuperBowl("SuperBowlII","04/02/2018",
				"Minnesota, United States","The 52nd Super Bowl",
				"New England Patriots","Philadephia Eagles","Philadephia Eagles");
		superBowlII.setDescriptionAndRules();
		System.out.println(superBowlII);
		
		SuperBowl superBowlLI = new SuperBowl("SuperBowlLI","05/02/2017","Taxes, United States",
				"the 52st Super Bowl",
				" New England Patriots",
				"Philadelphia Eagles","New England Patriots");
		System.out.println(superBowlLI);
		
		ThailandGoTournament the20thGoOpen = new ThailandGoTournament("THE 20th THAILAND OPEN GO TOURNAMENT",
				"20/08/2017","Samut Prakan, Thailand");
		the20thGoOpen.setWinner("������  High kyu �ҧ����ͧ��������ѹ�Ѻ  2 ����   ����� "
				+ "   ��Ƿ��¢��");
		the20thGoOpen.setDescriptionAndRules();
		System.out.println(the20thGoOpen);
	}
}
