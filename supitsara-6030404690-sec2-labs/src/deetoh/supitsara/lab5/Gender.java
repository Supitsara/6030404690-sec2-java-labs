package deetoh.supitsara.lab5;
/**<h1> Gender </h1>
 * This program is called Gender .It contains gender enum MALE and FEMALE.
 * <p>
 * @author Ms.Supitsara Deetoh  603040469-0  sec2
 * @since 29 Jan 2018 */

public enum Gender {
	MALE,FEMALE;

}
