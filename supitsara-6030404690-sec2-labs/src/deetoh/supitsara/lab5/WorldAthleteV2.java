package deetoh.supitsara.lab5;
/**<h1> WorldAthleteV2 </h1>
 * This program will be get Badminton player,Footballer and boxer's infomation sample name ,weight(kg) ,height(m) ,gender(must choose MALE or FEMALE)
 * , nationality , birthdate(dd/MM/yyyy),racket lenght,world ranking ,division and glove size(Y,S,M,L and XL).
 * It'll buid 3 objects sample ratchanok, tom and wisaksil,then show.
 * In addition to ,it can compare age and height who is taller.
 * <p>
 * @author	Ms.Supitsara Deetoh
 * @version 2.0  603040469-0  sec2
 * @since	3Jan2018*/
public class WorldAthleteV2 {

	public static void main(String[] args) {
		BadmintonPlayer ratchanok = new BadmintonPlayer("Ratchanok Intanon", 
				55, 1.68, Gender.FEMALE, "Thai", "05/02/1995", 66.5,  4);
		
		Footballer tom = new Footballer("Tom Brady", 102, 1.93, Gender.MALE, 
				"American", "03/08/1977", "Quarterback",  "New England Patriots");
		
		Boxer wisaksil = new Boxer("Wisaksil Wangek", 51.5, 1.60, Gender.MALE,
				"Thai", "08/12/1986", "Super Flyweight", "M" );
		
		System.out.println(ratchanok);
		System.out.println(wisaksil);
		System.out.println(tom);
		
		BadmintonPlayer nitchaon = new BadmintonPlayer("Nitchaon Jindapol",
				52, 1.63, Gender.FEMALE, "Thailand", "31/03/1991", 67, 11);
		
		System.out.println("Both " + ratchanok.getName() + " and " + nitchaon.getName() + 
				" play " + BadmintonPlayer.getSport() );
		
		ratchanok.compareAge(tom);
		ratchanok.compareAge(nitchaon);

			if (isTaller(tom,nitchaon)) {
				System.out.print(wisaksil.getName()+" is taller than "+tom.getName());
			}
			else {
				System.out.print(tom.getName()+" is taller than "+wisaksil.getName());
				
			}
	}

	private static boolean isTaller(Footballer tom, BadmintonPlayer nitchaon) {
		return false;
	}

}
