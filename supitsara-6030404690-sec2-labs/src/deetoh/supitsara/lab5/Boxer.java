package deetoh.supitsara.lab5;
/**<h1> Boxer </h1>
 * This program is called Boxer
 * This program will be get Boxer's infomation sample name ,weight(kg) ,height(m) ,gender(must choose MALE or FEMALE)
 * , nationality , birthdate(dd/MM/yyyy),division and glove size(Y,S,M,L and XL) then enter information
 * <p>
 * @author Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 3 Jan 2018 */


public class Boxer extends Athlete {
	static String sport = "Boxing";
	private String division;
	private String gloveSize;
	public enum gloveSize { Y,S,M,L,XL }
	
	@Override
	public void playSport() {
		System.out.print(getName()+" goot at "+sport);
	}
	
	/**constructor*/
	/**
	 @param name Firstname and lastname
	 * @param weight Weight in kilograms
	 * @param height Height in meters
	 * @param gender Using enum with values as MALE or FEMALE
	 * @param nationality Full nationality name, such as Thai
	 * @param birthdate Date of birth in format 31/12/2017
	 * @param division level of champion 
	 * @param gloveSize size of glove in Y S M L XL
	 */
	public Boxer(String name, double weight, double height, Gender gender, String nationality, String birthdate,String division,String gloveSize) {
		super(name, weight, height, gender, nationality, birthdate);
		this.division = division;
		this.gloveSize = gloveSize;
	}
	

	
	/**To string*/
	@Override
	public String toString() {
		return getName()+", "+getWeight()+"kg, "+getHeight()+"m, "+getGender()+", "+getNationality()+", "+getBirthdate()+", "+sport+", "+division+", "+gloveSize;
	}
	
	@Override
	public void play() {
		System.out.println(getName()+" throws a punch");
		
	}
	@Override
	public void move() {
		System.out.println(getName()+" moves around a boxing ring.");
		
	}
	
	
}
