package deetoh.supitsara.lab5;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**<h1> BadmintonPlayer </h1>
 * This program is called BadmintonPlayer
 * This program will be get BadmintonPlayer's infomation sample name ,weight(kg) ,height(m) ,gender(must choose MALE or FEMALE)
 * , nationality , birthdate(dd/MM/yyyy),racketLenght and WorldRanking then enter information .
 * This program has compare age method .It's compare by year.
 * <p>
 * @author Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 3 Jan 2018 */

public class BadmintonPlayer extends Athlete {
	static String sport = "Badminton";
	private double racketLenght;
	private int WorldRanking;
	
	/**constructor*/
	
	/**@param name Firstname and lastname
	 * @param weight Weight in kilograms
	 * @param height Height in meters
	 * @param gender Using enum with values as MALE or FEMALE
	 * @param nationality Full nationality name, such as Thai
	 * @param birthdate Date of birth in format 31/12/2017
	 * @param racketLenght Length of the racket
	 * @param WorldRanking world ranking of this Badminton player
	 */
	public BadmintonPlayer(String name, double weight, double height, Gender gender, String nationality,
			String birthdate,double racketLenght,int WorldRanking) {
		
		super(name, weight, height, gender, nationality, birthdate);
		this.racketLenght = racketLenght;
		this.WorldRanking = WorldRanking;
	
	}
	
	/**Getter and setter*/
	/**
	 * 
	 * @return name sport
	 */
	public static String getSport() {
		return sport;
	}

	public static void setSport(String sport) {
		BadmintonPlayer.sport = sport;
	}

	public double getRacketLenght() {
		return racketLenght;
	}

	public void setRacketLenght(double racketLenght) {
		this.racketLenght = racketLenght;
	}

	public int getWorldRanking() {
		return WorldRanking;
	}

	public void setWorldRanking(int worldRanking) {
		WorldRanking = worldRanking;
	
	}
	
	@Override
	public void playSport() {
		System.out.println( getName() +" is goot at "+ BadmintonPlayer.getSport());
	}


	@Override
	/**To string*/
	public String toString() {
		return getName()+", "+getWeight()+"kg, "+getHeight()+"m, "+getGender()+", "+getNationality()+", "+getBirthdate()+", "+sport+", "+ racketLenght +"cm, rank:" + WorldRanking;
	}
	

	public void compareAge(Athlete player) {
		LocalDate dateBefore = super.getBirthdate();
		LocalDate dateAfter = player.getBirthdate();
		long year = ChronoUnit.YEARS.between(dateBefore,dateAfter);
		
		if (year > 0 ) {
			System.out.printf("%s is %d years older than %s %n",super.getName(),Math.abs(year),player.getName());
		}
		else if (year < 0) {
			System.out.printf("%s is %d years older than %s %n",player.getName(),Math.abs(year),super.getName());
		}
		
	}
	@Override
	public void play() {
		System.out.println(getName()+" hits a shuttlecock.");
		
	}
	@Override
	public void move() {
		System.out.println(getName()+" moves around badminton court.");
		
	}

}
