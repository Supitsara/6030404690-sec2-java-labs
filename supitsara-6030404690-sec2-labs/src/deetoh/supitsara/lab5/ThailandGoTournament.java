package deetoh.supitsara.lab5;
/**<h1> ThailandGoTournament </h1>
 * there is a new variable ,winnerDesc,
 * getter and setter new variables,
 * setDescriptionAndRules method do different from this one from SuperBowl class,
 * 2 constructors do different fuction,
 * toString method that returns text.
 * <p>
 * @author Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 9 Feb 2018 */
import java.time.LocalDate;

public class ThailandGoTournament extends Competition {
	public String winnerDesc;
	
	void setDescriptionAndRules() {
		System.out.println("===== Begin: Description and Rules ================");
		System.out.println("The competition is open for all students and people");
		System.out.println("The competition is divided into four categories");
		System.out.println("1. High Dan (3 Dan and above)");
		System.out.println("2. Low Dan (1-2 Dan)");
		System.out.println("3. High Kyu (1-4 Kyu)");
		System.out.println("4. Low Kyu (5-8 Kyu)");
		System.out.println("===== End: Description and Rules ================");
	}
	
	public void setWinner(String winnerDesc) {
		this.winnerDesc = winnerDesc;
	}
	public ThailandGoTournament(String name, String date, String place) {
		this.name = name;
		this.place = place;
		this.date = LocalDate.parse(date,formatter);
	}
	
	public ThailandGoTournament(String name,String date, String place,String winnerDesc) {
		this.winnerDesc = winnerDesc;
		this.name = name;
		this.place = place;
		this.date = LocalDate.parse(date,formatter);
	}
	
	

	public String getWinnerDesc() {
		return winnerDesc;
	}
	@Override
	public String toString() {
		return name+" was held on "+date+" in "+place+"\n"+"Some winners are "+winnerDesc;
	}
}
