package deetoh.supitsara.lab6.advanced;
/**<h1> Book </h1>
 * This program is called Book ,there are 2 constructer, getter and setter method,
 * 3 method .there're  listAuthor(),getAuthorNames(), and toString. 
 * and 4 private variable there're name, authors, price, qty.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 16 Feb 2018 */
import deetoh.supitsara.lab6.Author;

public class Book {
	private String name;
	private Author[] authors;
	private double price;
	private int qty;
	
	
	public Book(String name, Author[] authors, double price) {
		super();
		this.name = name;
		this.authors = authors;
		this.price = price;
	}
	public Book(String name, Author[] authors, double price, int qty) {
		super();
		this.name = name;
		this.authors = authors;
		this.price = price;
		this.qty = qty;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Author[] getAuthors() {
		return authors;
	}
	public void setAuthors(Author[] authors) {
		this.authors = authors;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	
	public String listAuthor(Author[] authors) {
		int i;
		String blank = authors[0].toString();
		int lengtharr = authors.length;
			
		for(i=1;i<(lengtharr);i++) {
			blank += ", \n"+authors[i].toString();
		}
		
		return blank;
	}
	public String getAuthorNames() {
		String name = authors[0].getName() ;
		int i ;
		int lengtharr = authors.length;
		for(i=1;i<(lengtharr);i++) {
		name += ", "+authors[i].getName();
		}
		return name;
	}
		
	@Override
	public String toString() {
		return "Book [name=" + name + ", \nauthors={" + listAuthor(authors) +"},price=" + price + ", qty=" + qty
				+ "]";
	}
}
