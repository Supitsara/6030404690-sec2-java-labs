package deetoh.supitsara.lab6.advanced;
/**<h1> TestAuthors </h1>
 * This program is called TestAuthors
 * A book can written by one or more authors. this program displays authors name , email and gender.
 * then show price book and quantity.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 16 Feb 2018 */
import deetoh.supitsara.lab6.Author;

public class TestAuthors {
	public static void main(String[] args) {
		// Declare and allocate an array of Authors
		Author[] authors = new Author[2];
		authors[0] = new Author("James Gosling", "james@somewhere.com", 'm');
		authors[1] = new Author("Ken Arnold", "ken@nowhere.com", 'm');

		// Declare and allocate a Book instance
		Book java = new Book("The Java programming language", authors, 19.99, 99);
		System.out.println(java);  // toString()
        System.out.println("=== Author names of this book are ===");
		System.out.println(java.getAuthorNames());
	}
}

