package deetoh.supitsara.lab6;
/**<h1> Book </h1>
 * This program is called Book ,there are variables, 2 constructer, getter and setter method and toString. 
 * and 6 private variable there're name, author, price, qty, email and gender.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 16 Feb 2018 */
public class Book {
	private String name;
	private Author author;
	private double price;
	private int qty = 0;
	private String email;
	private char gender;
	
	public Book(String name, Author author, double price,int qty) {
		this.name = name;
		this.author = author;
		this.price = price;
		this.qty = qty;
	}
	
	public Book(String name, Author author, double price) {
		this.name = name;
		this.author = author;
		this.price = price;
	}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Author getAuthor() {
		return author;
	}
	public void setAuthor(Author author) {
		this.author = author;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public char getGender() {
		return gender;
	}
	public void setGender(char gender) {
		this.gender = gender;
	}
	@Override
	public String toString() {
		return "Book [name = " + name + ", " + author + ", price = " + price + ", qty = " + qty + "]";
	}
	
	
}