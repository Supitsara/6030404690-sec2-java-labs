package deetoh.supitsara.lab6;
/**<h1> WorldAthleteV5 </h1>
 * This program is called WorldAthleteV5.
 * The program cantains a static variable (NUM_PLAYERS),
 * a loop and arrays of new object.
 * then display Athlete's infomation ,
 * What do they hit?and call?
 * if Athlete is Thai BadmintonPlayer hits �١����,shuttlecock,
 * else they hit shuttlecock.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 16 Feb 2018 */
import deetoh.supitsara.lab5.BadmintonPlayer;
import deetoh.supitsara.lab5.Gender;

public class WorldAthleteV5 {
	public static void main(String[] args) {
		final int NUM_PLAYERS = 3;
		BadmintonPlayer[] players = new BadmintonPlayer[NUM_PLAYERS];
		players[0] = new ThaiBadmintonPlayer("Ratchanok Intanon", 
				55, 1.68, Gender.FEMALE, "05/02/1995", 66.5,  4);
		players[1] = new BadmintonPlayer("Tai Tzu-Ying", 
				57, 1.62, Gender.FEMALE, "Taiwan", "20/06/1994", 67.0,  1);
		players[2] = new ThaiBadmintonPlayer("Boonsak Ponsana", 
				72, 1.8, Gender.MALE, "22/02/1982", 70, 127);
		for (int i = 0; i < NUM_PLAYERS; i++) {
			System.out.println(players[i]);
			players[i].play();
		}
	}
	
}
