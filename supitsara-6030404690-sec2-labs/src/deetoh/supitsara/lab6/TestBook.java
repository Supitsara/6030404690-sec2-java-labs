package deetoh.supitsara.lab6;
/**<h1> TestBook </h1>
 * This program is called TestBook ,
 * then displays book's infomation that author is written.
 * Book info,There are author'info, book's name, price and quantity.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 16 Feb 2018 */
public class TestBook {

	public static void main(String[] args) {
		// Construct an author instance
		Author win = new Author("�Թ��� ��������Գ", "win@winbookclub.com", 'm'); // Test the constructor
		System.out.println(win);  // Test toString()

		Book democracyBook = new Book("��ЪҸԻ�º���鹢�ҹ", win, 19.95, 99);  // Test Book's Constructor
		System.out.println(democracyBook);  // Test Book's toString()
		 
		// Test Getters and Setters
		democracyBook.setPrice(29.95);
		democracyBook.setQty(28);
		System.out.println("name is: " + democracyBook.getName());
		System.out.println("price is: " + democracyBook.getPrice());
		System.out.println("qty is: " + democracyBook.getQty());
		System.out.println("Author is: " + democracyBook.getAuthor());  // Author's toString()
		System.out.println("Author's name is: " + democracyBook.getAuthor().getName());
		System.out.println("Author's email is: " + democracyBook.getAuthor().getEmail());

		// Use an anonymous instance of Author to construct a Book instance
		Book anotherBook = new Book("������ǡѹ", 
		      new Author("�ҳԪ ��ا�Ԩ͹ѹ��", "wanich@gmail.com", 'm'), 29.95);
		
		anotherBook.setPrice(29.95);
		System.out.println(anotherBook);  // toString()
	}
}