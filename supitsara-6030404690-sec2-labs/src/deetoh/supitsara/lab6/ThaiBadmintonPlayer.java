package deetoh.supitsara.lab6;
import deetoh.supitsara.lab5.BadmintonPlayer;
import deetoh.supitsara.lab5.Gender;
/**<h1> ThaiBadmintonPlayer </h1>
 * This program is called ThaiBadmintonPlayer ,
 * It's a subclass of BadmintonPlayer
 * It return equipment ("�١����") if Athlete is Thai Badminton player.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 16 Feb 2018 */
public class ThaiBadmintonPlayer extends BadmintonPlayer {
	private String equipment = "�١����"; 
	
	public ThaiBadmintonPlayer(String name, double weight, double height, Gender gender,
			String birthdate, double racketLenght, int WorldRanking) {
		super(name, weight, height, gender,"thai",birthdate, racketLenght, WorldRanking);
	}
	@Override
	public void play() {
		super.play();
		System.out.println(getName()+" hits a "+equipment);	
	}
	@Override
	public String toString() {
		return getName()+", "+getWeight()+"kg, "+getHeight()+"m, "+getGender()+", "+getNationality()+", "+getBirthdate()+", "+getRacketLenght()+", rank:"+getWorldRanking()+" equipment:"+equipment;
	}	
}
