package deetoh.supitsara.lab6;
/**<h1> Author </h1>
 * This program is called Author.
 * The program cantains 3 private variable (There're name, email and gender.),
 * a constructer, getter and setter method and toString method. 
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 16 Feb 2018 */

public class Author {
	private String name;
	private String email;
	private char gender;
	
	public Author(String name, String email, char gender) {
		this.name = name;
		this.email = email;
		this.gender = gender;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public char getGender() {
		return gender;
	}
	public void setGender(char gender) {
		this.gender = gender;
	}
	@Override
	public String toString() {
		return "Author [name = " + this.name + ", email = " + email + ", gender = " + gender + "]";
	}


}

