package deetoh.supitsara.lab6;
/**<h1> TestAuthor </h1>
 * This program is called TestAuthor ,
 * this program creates win(objects) that's Author class.
 * then displays author's infomation.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 16 Feb 2018 */
public class TestAuthor {
	public static void main(String[] args) {
		Author win = new Author("�Թ��� ��������Գ", "win@winbookclub.com", 'm'); // Test the constructor
		System.out.println(win);  // Test toString()
		win.setEmail("win@gmail.com");  // Test setter
		System.out.println("name is: " + win.getName());     // Test getter
		System.out.println("email is: " + win.getEmail());   // Test getter
		System.out.println("gender is: " + win.getGender()); // Test gExerciseOOP_MyPolynomial.pngetter
	}
}
