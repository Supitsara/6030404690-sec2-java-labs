/**This Program's called a dice game.
 * It's random number game (1 to 6).
 * There're 1 player that plays with computer.
 * How to play ? : enter 1 number sample 1,2,...,6. then the program'll show random number (1-6),
 * if your number close to random number about 1 or it's equal to random number, You'll win.
 * Author : Ms.Supitsara Deetoh
 * ID : 603040469-0
 * sec : 2
 Date : 22 Jan 2018*/

package deetoh.supitsara.lab3;

import java.util.Scanner;
import java.util.Random;

public class DiceGame {
	public static void main(String[] x) {
		/**
		 * This line get guest number from keyboard. run and enter at console
		 */
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter your guest (1-6):");
		int s = sc.nextInt();
		sc.close();

		if (s < 1) {
			System.err.println("Incorrect number. Only 1-6 can be entered.");
		} else if ((s >= 1) && (s <= 6)) {
			/****
			 * Notice : describe This conditional your number is 6 . if random number are 5
			 * , 6 or 1 ,you'll win. your number is 1 .if random number are 6 , 1 or 2
			 * ,you'll win as well.
			 */
			System.out.println("You have guest number :" + s);
			Random Rand = new Random();
			int reSultRand = Rand.nextInt(6) + 1;
			System.out.println("Computer has rolled number :" + reSultRand);

			if ((Math.abs(reSultRand - s) <= 1) || (Math.abs(reSultRand - s) == 5)) {

				System.out.print("You win");
			} else {
				System.out.print("Computer wins");
			}
		} else {
			System.err.println("Incorrect number. Only 1-6 can be entered.");
		}

	}
}
