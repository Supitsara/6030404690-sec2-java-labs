/**This program 's called a DiceMethodGame. It's dice game but this program structer is done by method.
 * you can play this game with computer.playing this game by enter number 1 to 6.
 * then computer'll random 1 number ,like you.
 * rulse : if your number close to random number about 1 or it's equal to random number, You'll win.
 * Notice : describe This conditional your number is 6 . if random number are 5, 6 or 1 ,you'll win.
 * 			your number is 1 .if random number are 6 , 1 or 2 ,you'll win as well.
 * 
 * Author : Ms.Supitsara Deetoh
 * ID : 603040469-0
 * sec : 2
 Date : 22 Jan 2018*/

package deetoh.supitsara.lab3;

/**This Program 's called a dice game.
 *  
 * Author : Ms.Supitsara Deetoh
 * ID : 603040469-0
 * sec : 2
 * Date : 22 Jan 2018*/
import java.util.Random;
import java.util.Scanner;

public class DiceMethodGame {
	static int humanGuest;
	static int computerScore;
	public static void main(String[] x) {
		humanGuest = acceptInput();
		computerScore = genDiceRoll();
		displayWinner (humanGuest, computerScore);
	}
	public static int acceptInput() {
		Scanner sc = new Scanner (System.in);
		System.out.print("Enter your guest (1-6):");
		int s = sc.nextInt();
		sc.close();
		if ((s < 1)||(s>6)) {
			System.err.println("Incorrect number. Only 1-6 can be entered.");
			System.exit(0);
		}
		return s;
		
	}
	public static int genDiceRoll() {
		Random Rand = new Random();
		int reSultRand = Rand.nextInt(6) + 1;
		return reSultRand;
	}
	public static void displayWinner(int humanGuest,int computerScore){
		System.out.println("You have guest number :"+humanGuest);
		System.out.println("Computer has rolled number :"+computerScore);
			if ((Math.abs(computerScore-humanGuest)<=1) || (Math.abs(computerScore-humanGuest)==5)){
				
				System.out.print("You win");
			}
			else {
				System.out.print("Computer wins");
			}
	}

}
