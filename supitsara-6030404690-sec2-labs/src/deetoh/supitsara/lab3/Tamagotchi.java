/**This program is called Tamagotchi.Like you've to take care of digital pet.
 * It's program that you enter 3 int numbers from terminal window,then calculate times to feed and water.
 * times mean feed and water per hour.you can do it at the same time if you have times to feed and water.
 * 3 ints number that you enter are live ,fed and watered.
 * Author : Ms.Supitsara Deetoh
 * ID : 603040469-0
 * sec : 2
 Date : 22 Jan 2018*/

package deetoh.supitsara.lab3;
import java.util.Scanner;

public class Tamagotchi {
	public static void main(String[] x) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter your tamagot info :");
		
		int live = scanner.nextInt(); /**got life hour of Tamagotchi*/
		int feed = scanner.nextInt(); /**got hours to fed */
		int waTer = scanner.nextInt(); /**got hours to watered */
		
		System.out.printf("Your Tamagotchi will live for %d hours %n",live);
		System.out.printf("It needs to be fed for %d hours %n",feed);
		System.out.printf("It needs to be watered for %d hours %n%n",waTer);
		
		scanner.close();
		

		int hwaTerAndFeed = live/(feed*waTer);	/**got hour feed and water at the same time*/
		int hWaTer = live/waTer
			,hFeed = live/feed;	/**got full hour to water,feed */
		int hourToWater = hWaTer-hwaTerAndFeed; /**got hour to water */
		int hourToFeed = hFeed-hwaTerAndFeed; /**got hour to feed */
			
		System.out.printf("You need to water-feed: %d times %n",hwaTerAndFeed);
		System.out.printf("You need to water: %d times %n",hourToWater);
		System.out.printf("You need to feed: %d times %n",hourToFeed);

	}
}