package deetoh.supitsara.lab9;

/**<h1> AthleteFormV6 </h1>
 * This program call AthleteFormV5.It has image icon in menuitem open ,save ,exit.
 * And Images on top center.
 * and the same thing, when you click OK it'll show message dialog that show the info you input.
 * if you click cancel ,it'll show the terminal that you look first.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 1 April 2018 */
import java.awt.BorderLayout;
import javax.swing.*;

public class AthleteFormV6 extends AthleteFormV5 {

	private static final long serialVersionUID = 1L;
	protected JMenuItem New,open,save,exit;
	protected JLabel pic;
	protected JPanel mainPanelV6;
	ImageIcon icon ;
	public AthleteFormV6(String title) {
		super(title);
	}
	
	@Override
	public void addComponents() {
		super.addComponents();
		JPanel mainPanelV6 = new JPanel(new BorderLayout(10,10) );
		
		New = new JMenuItem("New");
		open = new JMenuItem("Open",new ImageIcon("images/openIcon.png"));
		save = new JMenuItem("Save",new ImageIcon("images/saveIcon.png"));
		exit = new JMenuItem("Exit",new ImageIcon("images/quitIcon.png"));
		file.add(New);
		file.add(open);
		file.add(save);
		file.add(exit);
		
		icon = new ImageIcon("images/mana.jpg");
		pic = new JLabel(icon);
		
		mainPanelV6.add(jmb,BorderLayout.PAGE_START);
		mainPanelV6.add(pic,BorderLayout.CENTER);
		mainPanelV6.add(subPanelV5,BorderLayout.PAGE_END);
		setContentPane(mainPanelV6);
	}
	
	public static void createAndShowGUI(){
		AthleteFormV6 AthleteV6 = new AthleteFormV6("Athlete Form V6");
		AthleteV6.addComponents();
		AthleteV6.setFrameFeatures();
		AthleteV6.addListeners();
	}
	
	public static void main(String[] arg) {
		SwingUtilities.invokeLater(new Runnable() {	
			public void run() {
				createAndShowGUI();	
			}
		});
	}
	

}
