package deetoh.supitsara.lab9;
/**<h1> AthleteFormV5 </h1>
 * This program call AthleteFormV5.
 * This program looks like the AthleteFormV5.But, if you choose color or size menu
 * it'll chage size and color in textfield , textarea.
 * and the same thing, when you click OK it'll show message dialog that show the info you input.
 * if you click cancel ,it'll show the terminal that you look first.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 1 April 2018 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class AthleteFormV5 extends AthleteFormV4 implements ActionListener {

	private static final long serialVersionUID = 1L;
	protected JMenuBar jmb;
	protected JMenu file,config,color,size;
	protected JPanel mainPanelV5 ,subPanelV5;
	//JMenuItem New,Open,Save,Exit;
	protected JMenuItem blue,green,red,custom,s16,s20,s24;
	public AthleteFormV5(String title) {
		super(title);
	}

	
	@Override
	public void addComponents() {
		super.addComponents();
		//mainPanel
		mainPanelV5 = new JPanel(new BorderLayout());
		subPanelV5 = new JPanel(new BorderLayout());
		//Add menu bar
		jmb = new JMenuBar();
		file = new JMenu("File");
		//New = new JMenuItem("New");
		//Open = new JMenuItem("Open");
		//Save = new JMenuItem("Save");
		//Exit = new JMenuItem("Exit");
		//file.add(New);
		//file.add(Open);
		//file.add(Save);
		//file.add(Exit);
		jmb.add(file);
		
		config = new JMenu("Config");
		//Add color menu:blue,green,red,custom... 
		color = new JMenu("Color");
		blue = new JMenuItem("Blue");
		green = new JMenuItem("Green");
		red = new JMenuItem("Red");
		custom = new JMenuItem("Custom...");//use again
		
		color.add(blue);
		color.add(green);
		color.add(red);
		color.add(custom);
		config.add(color);
		//Add color menu:16,20,24,custom... 
		size = new JMenu("Size");
		 s16 = new JMenuItem("16");
		s20 = new JMenuItem("20");
		s24 = new JMenuItem("24");
		size.add(s16);
		size.add(s20);
		size.add(s24);
		size.add(custom);
		
		config.add(size);	
		jmb.add(config);
		
		//Addmenubar to mainPanelV5
		mainPanelV5.add(jmb,BorderLayout.PAGE_START);
		
			//Add Pane_1 from class AthleteFormV3 to subPanelV5
			subPanelV5.add(pane_1,BorderLayout.PAGE_START);
			//Add Pane_2 from class AthleteFormV3 to subPanelV5
			subPanelV5.add(pane_2,BorderLayout.PAGE_END);
		mainPanelV5.add(subPanelV5,BorderLayout.PAGE_END);
		
		setContentPane(mainPanelV5);
		
	}
	
	@Override
	protected void addListeners() {
		super.addListeners();
		//menu Color add action
		blue.addActionListener(this);
		green.addActionListener(this);
		red.addActionListener(this);
		custom.addActionListener(this);
		//size.addActionListener(this);
		s16.addActionListener(this);
		s20.addActionListener(this);
		s24.addActionListener(this);
	}

	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();
		super.actionPerformed(e);
		if (src == blue) {
			getColor(Color.blue);}
		else if (src == green) {
			getColor(Color.green);}
		else if (src == red) {
			getColor(Color.red);}
		
		if (src == s16) {
			setSize(16);}
		else if (src == s20) {
			setSize(20);}
		if (src == s24) {
			setSize(24);}	
	}
	public void getColor(Color paint) {
		box1.setForeground(paint);
		box2.setForeground(paint);
		box3.setForeground(paint);
		box4.setForeground(paint);
		box5.setForeground(paint);	
		competTextArea.setForeground(paint);
	}
	
	public void setSize(int fontsize) {
		box1.setFont(new Font("Serif", Font.BOLD, fontsize));
		box2.setFont(new Font("Serif", Font.BOLD, fontsize));
		box3.setFont(new Font("Serif", Font.BOLD, fontsize));
		box4.setFont(new Font("Serif", Font.BOLD, fontsize));
		box5.setFont(new Font("Serif", Font.BOLD, fontsize));
		competTextArea.setFont(new Font("Serif", Font.BOLD, fontsize));
	}
	
	
	public static void createAndShowGUI(){
		AthleteFormV5 AthleteV5 = new AthleteFormV5("Athlete Form V5");
		AthleteV5.addComponents();
		AthleteV5.setFrameFeatures();
		AthleteV5.addListeners();
	}
	
	public static void main(String[] arg) {
		SwingUtilities.invokeLater(new Runnable() {	
			public void run() {
				createAndShowGUI();	
			}
		});
	}
}
