package deetoh.supitsara.lab9;
/**<h1> AthleteFormV1 </h1>
 * This program will show window
 * left window show text per line (Name,Birthdate,Weigth,Heigth,Nationality)
 * rigth window show 5 textfield per line
 * and below main panel (left+right panel) ,there are OK and Cancel button like class MySimpleWindow
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 1 April 2018 */
import java.awt.*;
import javax.swing.*;

public class AthleteFormV1edit extends MySimpleWindow {
	private static final long serialVersionUID = 1L;
	//Declare to use.
	JPanel panel1,panel2,mainpanel,lowPanel;
	JLabel label1,label2,label3,label4,label5;
	protected JTextField box1,box2,box3,box4,box5;
	
	public AthleteFormV1edit(String title) {
		super(title); }
	
    public static void createAndShowGUI(){
    	AthleteFormV1edit athleteForm1 = new AthleteFormV1edit("Athlete Form V1");
    	athleteForm1.addComponents();
    	athleteForm1.setFrameFeatures(); }
    

    protected void addComponents() {
    	//define main panel to add label and button
    	mainpanel = new JPanel(new BorderLayout());
    	
    	//define panel1 to place at left main panel
    	//define label objects
    	panel1 = new JPanel(new GridLayout(5,1));
    	label1 = new JLabel("Name:");
    	label2 = new JLabel("Birthdate:");
    	label3 = new JLabel("Weight (kg.):");
    	label4 = new JLabel("Height (metre):");
    	label5 = new JLabel("Nationality:");
    	//add label objects to panel1 (right main panel)
    	panel1.add(label1);
    	panel1.add(label2);
    	panel1.add(label3);
    	panel1.add(label4);
    	panel1.add(label5);
    	
    	//define panel2 to place at rigth main panel
    	//define textfield 
    	panel2 = new JPanel(new GridLayout(5,1));
    	box1 = new JTextField(15);
    	box2 = new JTextField(15);
    	box2.setToolTipText("ex. 22.02.200");
    	box3 = new JTextField(15);
    	box4 = new JTextField(15);
    	box5 = new JTextField(15);
    
    	
    	//Add textfiled to panel2 (left main panel)
    	panel2.add(box1);
    	panel2.add(box2);
    	panel2.add(box3);
    	panel2.add(box4);
    	panel2.add(box5);
    	
    	super.addComponents();
    	//Add panel1 and panel2 to main panel.
    	mainpanel.add(panel1,BorderLayout.LINE_START);
    	mainpanel.add(panel2,BorderLayout.LINE_END);
    	mainpanel.add(panel,BorderLayout.PAGE_END);
    	
		this.setContentPane(mainpanel);
    }

    protected void setFrameFeatures() {
    	super.setFrameFeatures();
    }
    public static void main(String[] args) {
    	SwingUtilities.invokeLater(new Runnable() {
		public void run() {
			createAndShowGUI();
		}
	});
}
  }
