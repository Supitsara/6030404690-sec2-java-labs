package deetoh.supitsara.lab9;
/**<h1> AthleteFormV4 </h1>
 * This program call AthleteFormV4.
 * There're menubar on the top and textfield,radiobutton and textarea to enter your infomation.
 * when you click OK it'll show message dialog that show the info you input.
 * if you click cancel ,it'll show the terminal that you look first.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 31 March 2018 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import deetoh.supitsara.lab7.AthleteFormV3;

public class AthleteFormV4 extends AthleteFormV3 implements ActionListener {
	private static final long serialVersionUID = 1L;
	private String Gender;
	private String Type;
	
	public AthleteFormV4(String title) {
		super(title);}
	
	protected void addListeners() {
		OK.addActionListener(this);
		buttonCancel.addActionListener(this);
	}
	
	public static void createAndShowGUI() {
		AthleteFormV4 frame = new AthleteFormV4("Athlete Form V4");

		frame.addComponents();
		frame.setFrameFeatures();
		frame.addListeners();		
	}
	
	public void setFrameFeatures() {
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w)/2;
		int y = (dim.height - h)/2;
		setLocation(x,y);
	    setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
			Object src = event.getSource();
			
			if (src == OK) {
				handleOKbutton();
			} else if (src == buttonCancel) {
				box1.setText("");
				box2.setText("");
				box3.setText("");
				box4.setText("");
				box5.setText("");
			}
			
		}
	public void handleOKbutton() {
		JFrame frame = new JFrame();
		
		
		Object Type = null;
		Type = String.valueOf(type_Chooser.getSelectedItem());
		
		if(radioBut_Male.isSelected()) {
			Gender = "Male";
		}else {
			Gender = "Female";}
		
		String text = "Name: "+box1.getText()+",Birthdate: = "+box2.getText()+", Weight = "+box3.getText()+", Height = "+
						box4.getText()+", Nationality = "+box5.getText()+"\nGender = "+Gender+"\nCompettition = "+competTextArea.getText()+"\nType = "+Type;
		
		JOptionPane.showMessageDialog(frame,text,"Message",JOptionPane.INFORMATION_MESSAGE);
	}
	
	public static void main(String[] arg) {
		SwingUtilities.invokeLater(new Runnable() {	
			public void run() {
				createAndShowGUI();		
			}
		});
	}

}
