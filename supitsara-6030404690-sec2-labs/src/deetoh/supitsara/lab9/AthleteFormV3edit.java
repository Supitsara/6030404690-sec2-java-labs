package deetoh.supitsara.lab9;
/**<h1> AthleteFormV3 </h1>
 * This program will show window
 * title window is Athlete Form V3
 * Top of this window , there is menubar (File,Config)
 * 		2 menu bar is file and config.
 * 			file : there are 4 menu item (New, Open, Save, Exit)
 * 			Config: ther are 2 menu item (Colors, Size)
 *  * there are 6 label , 6 textfield,Gender (Male or female),
 * text area to decribe compettition 
 * and type of athlete.(Badminton Player,Footballer and Boxer)
 * By the end of this window there are 2 button 
 * (Ok and cancel) like programe my simplewindow
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 1 April 2018 */
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

@SuppressWarnings("serial")
public class AthleteFormV3edit extends AthleteFormV2edit {
	protected JPanel pane_1,pane_2;
	protected JComboBox type_Chooser;
	protected JPanel combo_Box_panel, main_Panel3, type_Panel;
	protected JMenuBar type;
	protected JLabel type_la;
	protected String[] sport_Type[];
	protected JMenuItem New,Open,Save,Exit;
	
	public AthleteFormV3edit(String title) {
		super(title);
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
		public void run() {
			createAndShowGUI();}
		});

	}
	public void addComponents() {
		super.addComponents();
			main_Panel3 = new JPanel(new BorderLayout());
			JMenuBar menu_Bar = new JMenuBar();
			JMenu File = new JMenu("File");
			JMenu Config = new JMenu("Config");
			menu_Bar.add(File);
			menu_Bar.add(Config);
			
			New = new JMenuItem("New");
			Open = new JMenuItem("Open");
			Save = new JMenuItem("Save");
			Exit = new JMenuItem("Exit");
			File.add(New);
			File.add(Open);
			File.add(Save);
			File.add(Exit);
			
			JMenuItem Color = new JMenuItem("Color");
			Config.add(Color);
			JMenuItem Size = new JMenuItem("Size");
			Config.add(Size);
			
			
			
		main_Panel3.add(menu_Bar,BorderLayout.PAGE_START);//AddPanelmenubar
		
		
			pane_1 = new JPanel(new BorderLayout());
			pane_1.add(athleteV1_Panel,BorderLayout.PAGE_START);
			pane_1.add(gender_Panel,BorderLayout.LINE_START);//Add2Panel
			pane_1.add(compet_Panel,BorderLayout.PAGE_END);
		
		main_Panel3.add(pane_1,BorderLayout.LINE_START);
			type_Panel = new JPanel(new GridLayout(1,2));
				JLabel type_La = new JLabel("Type :");
				type_Panel.add(type_La);
				String sport_Type[] = {"Badminton Player","Boxer","Footballer"};
				type_Chooser = new JComboBox(sport_Type);
				
				type_Chooser.setPreferredSize(new Dimension(10,20));
				type_Panel.add(type_Chooser);
			pane_2 = new JPanel(new BorderLayout());
				pane_2.add(type_Panel,BorderLayout.PAGE_START);
				pane_2.add(panel,BorderLayout.PAGE_END);
				
		main_Panel3.add(pane_2,BorderLayout.PAGE_END);
		
		this.setContentPane(main_Panel3);
	}
	
	
	public static void createAndShowGUI(){
		AthleteFormV3edit AthleteForm3 = new AthleteFormV3edit("Athlete Form V3");
		AthleteForm3.addComponents();
		AthleteForm3.setFrameFeatures();
		
	}
	
	public void setFrameFeatures() {
			Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
			this.pack();
			int w = getSize().width;
			int h = getSize().height;
			int x = (dim.width - w)/2;
			int y = (dim.height - h)/2;
			setLocation(x,y);
		    setVisible(true);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}

	
}
