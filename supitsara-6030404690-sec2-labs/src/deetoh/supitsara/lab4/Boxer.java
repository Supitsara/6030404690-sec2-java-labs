package deetoh.supitsara.lab4;
/**This program is called Boxer
 * This program will be get Boxer's infomation sample name ,weight(kg) ,height(m) ,gender(must choose MALE or FEMALE)
 * , nationality , birthdate(dd/MM/yyyy),division and glove size(Y,S,M,L and XL) then enter information
 * 
 * Author : Ms.Supitsara Deetoh
 * ID : 603040469-0
 * sec : 2
 Date : 3 Jan 2018*/


public class Boxer extends Athlete {
	static String sport = "Boxing";
	private String division;
	private String gloveSize;
	public enum gloveSize { Y,S,M,L,XL }
	
	/**constructor*/
	public Boxer(String name, double weight, double height, Gender gender, String nationality, String birthdate,String division,String gloveSize) {
		super(name, weight, height, gender, nationality, birthdate);
		this.division = division;
		this.gloveSize = gloveSize;
	}
	
	/**To string*/
	@Override
	public String toString() {
		return getName()+", "+getWeight()+"kg, "+getHeight()+"m, "+getGender()+", "+getNationality()+", "+getBirthdate()+", "+sport+", "+division+", "+gloveSize;
	}
	
	
	
	
}
