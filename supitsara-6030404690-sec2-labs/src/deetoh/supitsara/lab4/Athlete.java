package deetoh.supitsara.lab4;

/**This program is called Athlete
 * This program will be get athlete's infomation sample name ,weight(kg) ,height(m) ,gender(must choose MALE or FEMALE)
 * , nationality and birthdate(dd/MM/yyyy) then enter information ,the program'll show info that athlete enter in the form.
 * 
 * Author : Ms.Supitsara Deetoh
 * ID : 603040469-0
 * sec : 2
 Date : 29 Jan 2018*/

import java.time.LocalDate;
import java.time.format.*;
public class Athlete {
	
	private String name;
	private double weight;
	private double height;
	private Gender gender; 
	private String nationality;
	private LocalDate birthdate;
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	
	public Athlete(String name, double weight, double height, Gender gender, String nationality, String birthdate) {
		super();
		this.name = name;
		this.weight = weight;
		this.height = height;
		this.gender = gender;
		this.nationality = nationality;	
		this.birthdate = LocalDate.parse(birthdate,formatter);
	}
		
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public LocalDate getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}
	@Override
	public String toString() {
		return "Athete [" + name + ", " + weight + "kg, " + height + "m, " + gender
				+ ", " + nationality + ", " + birthdate + "]";
	}
	
}
