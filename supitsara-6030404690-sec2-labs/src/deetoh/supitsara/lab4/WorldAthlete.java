/**This program is called WorldAthlete
 * It'll build 3 object and show Athlete's information sample name , height.
 * and show 3 objects.
 * Author : Ms.Supitsara Deetoh
 * ID : 603040469-0
 * sec : 2
 Date : 29 Jan 2018*/
package deetoh.supitsara.lab4;

public class WorldAthlete {

	public static void main(String[] args) {
		Athlete ratchanok = new Athlete("Ratchanok Intanon", 55, 1.68, Gender.FEMALE, 
				"Thai", "05/02/1995");
		Athlete wisaksil = new Athlete("Wisaksil Wangek", 51.5, 1.60, Gender.MALE,
				"Thai", "08/12/1986");
		Athlete tom = new Athlete("Tom Brady", 102, 1.93, Gender.MALE, 
				"American", "03/08/1977");
		System.out.println(ratchanok.getName() + "'s height is "+ratchanok.getHeight());
		ratchanok.setHeight(1.70);
		System.out.println(ratchanok.getName() + "'s height has increased to " + 
				ratchanok.getHeight());
		System.out.println(ratchanok);
		System.out.println(wisaksil);
		System.out.println(tom);
	}
}