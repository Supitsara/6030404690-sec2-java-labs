package deetoh.supitsara.lab4;

/**This program is called Boxer
 * This program will be get Footballer's infomation sample name ,weight(kg) ,height(m) ,gender(must choose MALE or FEMALE)
 * , nationality , birthdate(dd/MM/yyyy),position and team then enter information
 * 
 * Author : Ms.Supitsara Deetoh
 * ID : 603040469-0
 * sec : 2
 Date : 3 Jan 2018*/

public class Footballer extends Athlete {
	static String sport = "American Football";
	private String position;
	private String team;
	public Footballer(String name, double weight, double height, Gender gender, String nationality, String birthdate,String position,String team) {
		super(name, weight, height, gender, nationality, birthdate);
		this.position = position;
		this.team = team;
	}
	
	public static String getSport() {
		return sport;
	}
	public static void setSport(String sport) {
		Footballer.sport = sport;
	}



	@Override
	public String toString() {
		return getName()+", "+getWeight()+"kg, "+getHeight()+"m, "+getGender()+", "+getNationality()+", "+getBirthdate()+", "+sport+", "+position+", "+team ;
	}
		
}
