package deetoh.supitsara.lab4;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**This program is called BadmintonPlayer
 * This program will be get BadmintonPlayer's infomation sample name ,weight(kg) ,height(m) ,gender(must choose MALE or FEMALE)
 * , nationality , birthdate(dd/MM/yyyy),racketLenght and WorldRanking then enter information .
 * This program has compare age method .It's compare by year.
 * Author : Ms.Supitsara Deetoh
 * ID : 603040469-0
 * sec : 2
 Date : 3 Jan 2018*/

public class BadmintonPlayer extends Athlete {
	static String sport = "Badminton";
	private double racketLenght;
	private int WorldRanking;
	
	/**constructor*/
	public BadmintonPlayer(String name, double weight, double height, Gender gender, String nationality,
			String birthdate,double racketLenght,int WorldRanking) {
		
		super(name, weight, height, gender, nationality, birthdate);
		this.racketLenght = racketLenght;
		this.WorldRanking = WorldRanking;
	
	}
	
	/**Getter and setter*/
	public static String getSport() {
		return sport;
	}

	public static void setSport(String sport) {
		BadmintonPlayer.sport = sport;
	}

	public double getRacketLenght() {
		return racketLenght;
	}

	public void setRacketLenght(double racketLenght) {
		this.racketLenght = racketLenght;
	}

	public int getWorldRanking() {
		return WorldRanking;
	}

	public void setWorldRanking(int worldRanking) {
		WorldRanking = worldRanking;
	
	}

	@Override
	/**To string*/
	public String toString() {
		return getName()+", "+getWeight()+"kg, "+getHeight()+"m, "+getGender()+", "+getNationality()+", "+getBirthdate()+", "+sport+", "+ racketLenght +"cm, rank:" + WorldRanking;
	}

	public void compareAge(Athlete player) {
		LocalDate dateBefore = super.getBirthdate();
		LocalDate dateAfter = player.getBirthdate();
		long year = ChronoUnit.YEARS.between(dateBefore,dateAfter);
		
		if (year > 0 ) {
			System.out.printf("%s is %d years older than %s %n",super.getName(),Math.abs(year),player.getName());
		}
		else if (year < 0) {
			System.out.printf("%s is %d years older than %s %n",player.getName(),Math.abs(year),super.getName());
		}
		
	}

}
