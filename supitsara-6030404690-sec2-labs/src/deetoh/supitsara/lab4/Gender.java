/**This program is called Gender .It contains gender enum MALE and FEMALE.
 * Author : Ms.Supitsara Deetoh
 * ID : 603040469-0
 * sec : 2
 Date : 29 Jan 2018*/
package deetoh.supitsara.lab4;

public enum Gender {
	MALE,FEMALE;

}
