package deetoh.supitsara.lab8;
/**<h1> Ball </h1>
 * This class call Ball.
 * There're 3 variable: r ,VelX ,VelY
 * ,getter , setter 's VelX ,VelY
 * and 1 Constructer( 3 parameter ).
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 25 March 2018 */
import java.awt.geom.Ellipse2D;

public class Ball extends Ellipse2D.Double {
	
	private static final long serialVersionUID = 1L;
	
	private int r ;//radius of circle
	protected int velX,velY;
	
	public int getVelX() {
		return velX;
	}
	public void setVelX(int velX) {
		this.velX = velX;
	}
	public int getVelY() {
		return velY;
	}
	public void setVelY(int velY) {
		this.velY = velY;
	}
	public int getR() {
		return r;
	}
	public void setR(int r) {
		this.r = r;
	}
	public Ball(double x,double y,int r) {
		super((double)x,(double)y,(double)r*2,(double)r*2);
		this.r = 2*r;
	}
	
}
