package deetoh.supitsara.lab8;
/**<h1> SimpleGameWindow </h1>
 * This class call SimpleGameWindow.
 * There're 1 Constructer (to set title of this window)
 * and main to set size and location this window
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 25 March 2018 */
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.*;

public class SimpleGameWindow extends JFrame {
	public static final int HEIGHT = 500;
	public static final int WIDTH = 650;
	private static final long serialVersionUID = 1L;
	
	public SimpleGameWindow(String title) {
		super(title);
	}
	protected static void createAndShowGUI() {
		SimpleGameWindow window = new SimpleGameWindow("My Simple Game Window");
		window.setFrameFeature();
	}
	protected void setFrameFeature() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.pack();
		setSize(650,500);
		int w = getSize().width;//width screen
		int h = getSize().height;//heigth screen
		int x = (dim.width - w)/2; //half width
		int y = (dim.height - h)/2; //half heigth
		
		setLocation(x,y);
	    setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	public static void main(String[] args) {
			SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						createAndShowGUI();
					}
			});
	}
}
