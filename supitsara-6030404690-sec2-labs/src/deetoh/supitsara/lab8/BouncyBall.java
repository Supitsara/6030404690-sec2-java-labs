package deetoh.supitsara.lab8;
/**<h1> BouncyBall </h1>
 * This class call BouncyBall.
 * There're no variable.(built here)
 * , 1 Constructer , run method(to run ball again),paint method (to paint ball)
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 25 March 2018 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

public class BouncyBall extends JPanel implements Runnable{
	private static final long serialVersionUID = 1L;
	Thread running;
	MovingBall ballY2;
	Rectangle2D.Double box;
	
	public BouncyBall() {
		setBackground(Color.GREEN);
		ballY2 = new MovingBall(600,300,20,2,2);
		running = new Thread(this);
		running.start();
	}
	@Override
	public void run() {
		while(true) {
			ballY2.x -= ballY2.getVelX();
			ballY2.y += ballY2.getVelY();
			
			if(ballY2.x == 0 ) {
				ballY2.velX = ballY2.velX*(-1);}
			else if(ballY2.x == 600 ) {
				ballY2.velX = ballY2.velX*(-1);}
			if(ballY2.y == 0 ) {
				ballY2.velY = ballY2.velY*(-1);
			}
			else if(ballY2.y == 430 ) {
				ballY2.velY = ballY2.velY*(-1);
			}
			
			repaint();
			getToolkit().sync();
			
			try{
				Thread.sleep(10);
			}
			catch(InterruptedException ex) {
				System.err.println(ex.getStackTrace());
			}
		}
	}
	public void paint(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		
		g2d.setColor(Color.YELLOW);
		g2d.fill(ballY2);	
	}
}
