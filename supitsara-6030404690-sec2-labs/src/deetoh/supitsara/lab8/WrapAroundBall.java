package deetoh.supitsara.lab8;
/**<h1> WrapAroundBall </h1>
 * This class call WrapAroundBall.
 * There're 5 variable.(built from class MovingBall)
 * , 1 Constructer (to set blue background ,start point ball)
 * ,run method(to run ball again when it's over this window),paint method (to paint ball)
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 25 March 2018 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;

public class WrapAroundBall extends JPanel implements Runnable {

	private static final long serialVersionUID = 1L;
	MovingBall ballY,ballR,ballG,ballB,ballW;
	Thread running;

	public WrapAroundBall() {
		this.setBackground(Color.BLUE);
		ballY = new MovingBall(250,0,20,2,2);
		ballR = new MovingBall(0,250,20,2,2);
		ballG = new MovingBall(200,0,20,2,2);
		ballB = new MovingBall(400,400,20,2,2);
		ballW = new MovingBall(610,100,20,2,2);
		
		running=new Thread(this);
		running.start();
	}

	@Override
	public void run() {
		while(true) {
			
			ballY.y += ballY.getVelX();
			ballR.x += ballR.getVelX();
			ballG.x += ballG.getVelX();
			ballG.y += ballG.getVelX();
			ballB.y -= ballB.getVelY();
			ballW.x -= ballW.getVelX();
			
			if ( ballY.y > 400) {
				ballY.y = 0;	}
			if ( ballR.x > 620) {
				ballR.x = 0;	}
			if ( ballG.x > 620|| ballG.y > 420) {
				ballG.x = 400;
				ballG.y = 0;}
			if ( ballB.y < -20) {
				ballB.y = 400;	}
			if ( ballW.x < -20) {
				ballW.x = 610;	}

			try {
				Thread.sleep(20);
			}
			catch(InterruptedException ex) {
				System.err.println(ex.getStackTrace());
			}
			
			repaint();
			this.getToolkit().sync();
		}
		
	}
	public void paint(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		
		g2d.setColor(Color.YELLOW);
		g2d.fill(ballY);
		
		g2d.setColor(Color.RED);
		g2d.fill(ballR);
		
		g2d.setColor(Color.GREEN);
		g2d.fill(ballG);
		
		g2d.setColor(Color.BLACK);
		g2d.fill(ballB);
		
		g2d.setColor(Color.WHITE);
		g2d.fill(ballW);
		
	}
	
}
