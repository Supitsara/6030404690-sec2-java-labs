package deetoh.supitsara.lab8;
/**<h1> PongPanel </h1>
 * This class call PongPanel.
 * There're 5 variable (built here):paddle1,paddle2,scores1,scores2 and ball
 * , 1 Constructer (to set blackground and start point of ball, paddle and scores)
 * ,paintComponent (to build ball)
 * ,to string (to change type scores)
 * and main to show this window and obj.
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 25 March 2018 */
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;

public class PongPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	//five variable
	private PongPaddle paddle1,paddle2;
	private int scores1 ;
	private int scores2 ;
	private Ball ball;
	
	//Add one constructor 
	public PongPanel() {
		this.setBackground(Color.BLACK);
		//setBackground(Color.black);
		//score init 0
		this.scores1 = 0;
		this.scores2 = 0;
		//PongPaddle(x,y,w,h)
		//Ball(x,y,r)
		//paddle,shuttle(15 pixel and the height should be 80 pixel.)
		this.paddle1 = new PongPaddle(0,(SimpleGameWindow.HEIGHT/2)-40,15,80);
		this.paddle2 = new PongPaddle(SimpleGameWindow.WIDTH-30,SimpleGameWindow.HEIGHT/2-40,15,80);
		this.ball = new Ball((SimpleGameWindow.WIDTH/2)-20,(SimpleGameWindow.HEIGHT/2)-20,20);
	}
	//@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		
		g2d.setColor(Color.WHITE);
		g2d.fill(paddle1);
		g2d.fill(paddle2);
		g2d.fill(ball);
		
		//LINE
		g2d.drawLine(15, 0, 15,SimpleGameWindow.HEIGHT);
		g2d.drawLine(325, 0, 325,SimpleGameWindow.HEIGHT);
		g2d.drawLine(620, 0, 620,SimpleGameWindow.HEIGHT);
		
		//Score
		g2d.setFont(new Font("Serif",Font.BOLD,48));
		g2d.drawString(toString(scores1), SimpleGameWindow.WIDTH/4 , 100);
		g2d.drawString(toString(scores2), (SimpleGameWindow.WIDTH*3/4) , 100);
		
	}

	public String toString(int score) {
		String x = Integer.toString(score);
		return x;
	}
	public static void main(String[] arg) {
		PongGameGUI pong = new PongGameGUI("Pong Game Window");
		PongPanel pongPane = new PongPanel();
		pong.add(pongPane);
		pong.setContentPane(pongPane);
		pong.setFrameFeature();
	} 



}
