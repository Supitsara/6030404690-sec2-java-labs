package deetoh.supitsara.lab8;
/**<h1> MovingBall </h1>
 * This class call MovingBall.
 * There're 2 variable: velX,velY
 * , 1 Constructer (get 5 parameter: x,y,r,velX,velY)
 * , getter and setter (velX,velY).
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 25 March 2018 */
public class MovingBall extends Ball {

	private static final long serialVersionUID = 1L;
	public int velX,velY;

	public int getVelX() {
		return velX;
	}
	public void setVelX(int velX) {
		this.velX = velX;
	}
	public int getVelY() {
		return velY;
	}
	public void setVelY(int velY) {
		this.velY = velY;
	}
	public MovingBall(double x, double y, int r, int velX, int velY) {
		super(x, y, r);
		this.velX=velX;
		this.velY=velY;
	}

}