package deetoh.supitsara.lab8;
/**<h1> TestBouncyBall </h1>
 * This class call TestBouncyBall.
 * There're 1 Constructer (to set title of this window)
 * method to set this window (background ,title)
 * and main (to run BouncyBall programe).
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 25 March 2018 */
import java.awt.Color;

import javax.swing.SwingUtilities;

public class TestBouncyBall extends SimpleGameWindow {

	private static final long serialVersionUID = 1L;
	public TestBouncyBall(String string) {
		super(string);
	}
	protected void addComponents() {
		setBackground(Color.GREEN);
		BouncyBall panel = new BouncyBall();
		setContentPane(panel);
	}
	protected static void createAndShowGUI() {
		TestBouncyBall window = new TestBouncyBall("Test Bouncy Ball");
		window.setFrameFeature();
		window.addComponents();
	}
	public static void main(String[] ars) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
