package deetoh.supitsara.lab8;
/**<h1> TestWrapAroundBall </h1>
 * This class call TestWrapAroundBall.
 * It will show running 5 balls in different ways (red,black,green,white,yellow) and blue background.
 * There're 1 Constructer (to set title of this window)
 * and main (to run WrapAroundBall programe).
 * <p>
 * @author : Ms.Supitsara Deetoh  603040469-0  sec2
 * @version 1.0
 * @since 25 March 2018 */
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class TestWrapAroundBall extends JFrame {
	
	private static final long serialVersionUID = 1L;
	static final int WIDTH = 600;
	static final int HEIGHT = 400;
	
	public TestWrapAroundBall(String title) {
		super(title);
	}
	
	protected void setFrameFeatures() {
		setSize(WIDTH,  HEIGHT);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w)/2;
		int y = (dim.height - h)/2;
		setLocation(x,y);
	    setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	protected void  addComponents() {
		WrapAroundBall bluePanel = new WrapAroundBall();
		this.setContentPane(bluePanel);
	}
	
	public static void createAndShowGUI() {
		TestWrapAroundBall window = new TestWrapAroundBall("Test Wrap Around Ball");
		window.addComponents();
		window.setFrameFeatures();
	}
	
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
